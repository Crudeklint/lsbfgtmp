from django.contrib import admin

from .models import Player, Game, Event, ImportanceRating, GameWish
from .models import News, Placement, BugReport, PlayerCreateToken
from .models import ScoreRecord, BoardGame, Rating
from .models import TournamentEvent, BoardGameEvent, GeneralEvent
from .models import Food, ExtraPoint, ImportanceRating2, MoneyTransaction

# Register your models here.


class GameAdmin(admin.ModelAdmin):
    list_display = ("name", "system", "added_by", "boxart", "youtube")


class GameWishadmin(admin.ModelAdmin):
    list_display = ("event", "game", "player", "rating")


class BugReportAdmin(admin.ModelAdmin):
    list_display = ("report", "added_by", "added_date")


class PlayerCreateTokenAdmin(admin.ModelAdmin):
    list_display = ("email", "expire_date")


class RatingAdmin(admin.ModelAdmin):
    list_display = ("content_type", "object_id", "player", "rating")


class FoodAdmin(admin.ModelAdmin):
    list_display = ("name", "added_by")


admin.site.register(Player)
admin.site.register(Game, GameAdmin)
admin.site.register(Event)
admin.site.register(ImportanceRating)
admin.site.register(ImportanceRating2)
admin.site.register(News)
admin.site.register(Placement)
admin.site.register(ScoreRecord)
admin.site.register(BoardGame)
admin.site.register(TournamentEvent)
admin.site.register(BoardGameEvent)
admin.site.register(GeneralEvent)
admin.site.register(ExtraPoint)
admin.site.register(MoneyTransaction)

admin.site.register(PlayerCreateToken, PlayerCreateTokenAdmin)
admin.site.register(BugReport, BugReportAdmin)
admin.site.register(GameWish, GameWishadmin)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Food, FoodAdmin)
