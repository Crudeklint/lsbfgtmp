import json
import urllib.request
import urllib.error
import datetime
import re
import xml.etree.ElementTree as ET

from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.contrib.auth.password_validation import validate_password
from django.core import validators
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils.html import conditional_escape, escape
from django.utils.safestring import mark_safe
from django.shortcuts import render, redirect, get_object_or_404

from .models import GameWish, Event, Player, Game, User, ImportanceRating
from .models import Placement, News, PlayerCreateToken, BoardGame
from .models import BoardGameEvent, GeneralEvent, Food, ExtraPoint
from .models import MoneyTransaction, ImportanceRating2

# +-------------------+
# | CUSTOM VALIDATORS |
# +-------------------+


def first_name_validator(data):
    if len(data) < 2:
        raise ValidationError("Förnamn måste vara minst två bokstäver!!")

    if re.search("\d", data):
        raise ValidationError("Bara efternamn får ha siffor!!")

    if not re.match("[A-Öa-ö0-9]", data):
        raise ValidationError("Sluta tramsa!! Riktigt namn!!")


def last_name_validator(data):
    if len(data) < 2:
        raise ValidationError("Efternam måste ha minst två bokstäver!!")


def gamertag_validator(data):
    if "s" in data:
        raise ValidationError('Alla "s" måste bytas ut mot "z"')


def password_validator(data):
    validate_password(data)


class SeEmailValidator(validators.EmailValidator):
    message = "Måste vara en riktig mailadress!!!"


class ChoiceFieldNoValidation(forms.ChoiceField):
    def validate(self, value):
        pass


class CustomClearableFileInput(forms.widgets.ClearableFileInput):
    clear_checkbox_label = "Ta bort"
    input_text = "Byt bild"
    template_name = "gamemanager/clearable_file_input.html"


# +-------+
# | FORMS |
# +-------+


class CustomAuthenticationForm(AuthenticationForm):
    username = UsernameField(
        label="Användarnamn", widget=forms.EmailInput(attrs={"autofocus": True})
    )


class MarkNewInstancesAsChangedModelForm(forms.ModelForm):
    def has_changed(self):
        """Returns True for new instances, calls super() for ones that exist in db.
        Prevents forms with defaults being recognized as empty/unchanged."""
        return not self.instance.pk or super().has_changed()


class GameCreateUpdateForm(forms.ModelForm):
    class Meta:
        model = Game
        exclude = ("added_by",)


class BoardGameCreateUpdateForm(forms.ModelForm):
    class Meta:
        model = BoardGame
        exclude = ("added_by",)


class BoardGameImportBGGForm(forms.Form):
    bgg_pattern = "^(?:https?:\/\/)?boardgamegeek.com/boardgame/([^\/]+)"
    bgg_href = forms.CharField(
        label="BGG URL",
        required=True,
        validators=[
            validators.RegexValidator(
                bgg_pattern, message="Måste vara en boardgamegeek-address"
            ),
        ],
    )

    def clean_bgg_href(self):
        pattern = self.bgg_pattern
        input = self.cleaned_data["bgg_href"]
        gameid = re.match(pattern, input).group(1)
        call_href = "https://boardgamegeek.com/xmlapi2/thing?id={}".format(gameid)

        try:
            with urllib.request.urlopen(call_href) as url:
                data_str = url.read().decode()

        except (urllib.error.URLError, urllib.error.HTTPError):
            raise forms.ValidationError(
                'Det går inte att ansluta till "{}"'.format(call_href)
            )

        tree = ET.fromstring(data_str)

        image = [x.text for x in tree.iter(tag="image")]
        maxplayers = [x.attrib["value"] for x in tree.iter(tag="maxplayers")]
        name = [x.attrib["value"] for x in tree.iter(tag="name")]
        description = [x.text for x in tree.iter(tag="description")]

        bgg_data = {
            "name": name[0],
            "header_image": image[0],
            "max_players": maxplayers[0],
            "description": description[0],
            "gameid": gameid,
        }

        return bgg_data


class GameImportSteamForm(forms.Form):
    steam_id_pattern = "^(?:(?:https?:\/\/)?store\.steampowered\.com\/app\/)?(\d+)"
    steam_href = forms.CharField(
        label="Steam ID",
        required=True,
        validators=[
            validators.RegexValidator(
                steam_id_pattern, message="Måste vara en steam-address eller steam-id"
            ),
        ],
    )

    def clean_steam_href(self):
        pattern = self.steam_id_pattern
        input = self.cleaned_data["steam_href"]
        gameid = re.match(pattern, input).group(1)
        call_href = "https://store.steampowered.com/api/appdetails?appids={}".format(
            gameid
        )

        try:
            with urllib.request.urlopen(call_href) as url:
                data = json.loads(url.read().decode())

            if data[gameid]["success"] == False:
                raise forms.ValidationError("Hittar inget sånt spel!")

                data = json.loads(url.read().decode())

        except (urllib.error.URLError, urllib.error.HTTPError):
            raise forms.ValidationError(
                'Det går inte att ansluta till "{}"'.format(call_href)
            )

        try:
            no_movies = len(data[gameid]["data"]["movies"])
        except KeyError:
            no_movies = 0

        if no_movies > 1:
            movie = data[gameid]["data"]["movies"][1]["mp4"]["max"]
        elif no_movies == 1:
            movie = data[gameid]["data"]["movies"][0]["mp4"]["max"]
        else:
            movie = ""
        steam_data = {
            "name": data[gameid]["data"]["name"],
            "movie": movie,
            "header_image": data[gameid]["data"]["header_image"],
            "gameid": gameid,
        }

        return steam_data


class PlacementForm(forms.ModelForm):
    def __init__(self, *args, event_id, **kwargs):
        self.event_id = event_id
        super().__init__(*args, **kwargs)

        self.fields["firstplace"].queryset = Event.objects.get(
            id=event_id
        ).players.all()
        self.fields["secondplace"].queryset = Event.objects.get(
            id=event_id
        ).players.all()
        self.fields["thirdplace"].queryset = Event.objects.get(
            id=event_id
        ).players.all()

    class Meta:
        model = Placement
        fields = ("id", "event", "game", "firstplace", "secondplace", "thirdplace")


class ExtraPointForm(forms.ModelForm):
    def __init__(self, *args, event_id, **kwargs):
        self.event_id = event_id
        super().__init__(*args, **kwargs)

        event_players = Event.objects.get(id=event_id).players.all()

        self.fields["player"].queryset = event_players
        self.fields["from_player"].queryset = event_players

    def save(self, commit=True):
        event = Event.objects.get(id=self.event_id)

        instance = super(ExtraPointForm, self).save(commit=False)
        instance.event = event
        if commit:
            instance.save()

        return instance

    class Meta:
        model = ExtraPoint
        fields = ("id", "points", "player", "description", "from_player")

        widgets = {
            "description": forms.TextInput(),
        }


class ClientExtraPointForm(forms.ModelForm):
    def __init__(self, *args, event_id, from_player_id, **kwargs):
        self.event_id = event_id
        self.from_player_id = from_player_id

        super().__init__(*args, **kwargs)

        event_players = Player.objects.filter(event=event_id).exclude(id=from_player_id)

        self.fields["player"].queryset = event_players

    def save(self, commit=True):
        event = Event.objects.get(id=self.event_id)
        from_player = Player.objects.get(id=self.from_player_id)

        instance = super(ClientExtraPointForm, self).save(commit=False)
        instance.event = event
        instance.from_player = from_player

        if commit:
            instance.save()

        return instance

    def clean_points(self):
        data = self.cleaned_data["points"]

        extrapoints = ExtraPoint.objects.filter(
            event=self.event_id, from_player=self.from_player_id
        )

        total = sum([abs(x.points) for x in extrapoints])

        if total + abs(data) > 10000:
            raise ValidationError("FÖRSÖKER DU FUSKA DIN GRIS??")

        return data

    class Meta:
        model = ExtraPoint
        fields = ("id", "points", "player", "description")

        widgets = {
            "description": forms.TextInput(),
        }


class EventUpdateForm(forms.ModelForm):
    class Meta:
        model = Event

        exclude = (
            "players",
            "cannot_attend",
            "added_by",
            "bracket_data",
            "status",
            "thirdplace_factor",
            "firstplace_factor",
            "secondplace_factor",
            "score",
        )

        widgets = {
            "games": forms.CheckboxSelectMultiple(attrs={}),
            "boardgames": forms.CheckboxSelectMultiple(attrs={}),
        }


class GeneralEventUpdateForm(forms.ModelForm):
    class Meta:
        model = GeneralEvent
        fields = (
            "type",
            "name",
            "location",
            "location_href",
            "info",
            "date",
            "date_preliminary",
        )


class BoardGameEventUpdateForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = (
            "type",
            "name",
            "location",
            "location_href",
            "info",
            "date",
            "date_preliminary",
            "boardgames",
        )

        widgets = {
            "boardgames": forms.CheckboxSelectMultiple(attrs={}),
        }


class PlayerUpdatePasswordForm(forms.Form):
    password1 = forms.CharField(
        label="Nytt lösenord",
        validators=[password_validator],
        widget=forms.PasswordInput,
    )

    password2 = forms.CharField(
        label="Repetera lösenord",
        validators=[password_validator],
        widget=forms.PasswordInput,
    )

    def clean_password2(self):
        data = self.cleaned_data["password2"]

        if self.cleaned_data["password1"] != data:
            raise ValidationError("Lösenorden misstämmer")

        return data


class PlayerUpdateForm(forms.Form):
    image = forms.ImageField(
        label="Profilbild", required=False, widget=CustomClearableFileInput
    )
    gamertag = forms.CharField(label="GamerTag")
    username = forms.EmailField(label="Email", validators=[SeEmailValidator()])
    first_name = forms.CharField(label="Förnamn", validators=[first_name_validator])
    last_name = forms.CharField(label="Efternamn", validators=[last_name_validator])

    NOTIFICATION_CHOICES = (
        ("event", "Event"),
        ("news", "Nyheter"),
    )

    notifications = forms.MultipleChoiceField(
        label="Notifikationer",
        choices=NOTIFICATION_CHOICES,
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )


class PlayerCreateForm(PlayerUpdateForm):
    token = forms.CharField(label="Token", widget=forms.HiddenInput, required=False)

    password1 = forms.CharField(
        label="Nytt lösenord",
        validators=[password_validator],
        widget=forms.PasswordInput,
    )

    password2 = forms.CharField(
        label="Repetera lösenord",
        validators=[password_validator],
        widget=forms.PasswordInput,
    )

    verified_mail = forms.BooleanField(label="Riktig mail")

    def clean_password2(self):
        data = self.cleaned_data["password2"]

        if self.cleaned_data["password1"] != data:
            raise ValidationError("Lösenorden misstämmer")

        return data

    def clean_username(self):
        data = self.cleaned_data["username"]

        if User.objects.filter(username=data).count() > 0:
            raise ValidationError("Användarnamnet finns redan")

        return data


class PlayerCreateWTokenForm(PlayerCreateForm):
    username = forms.EmailField(widget=forms.HiddenInput)
    verified_mail = forms.BooleanField(
        label="Riktig mail", widget=forms.HiddenInput, initial=True
    )


class GameSwapForm(forms.Form):
    def __init__(self, *args, event_id, **kwargs):
        super(GameSwapForm, self).__init__(*args, **kwargs)

        event = get_object_or_404(Event, pk=event_id)

        self.from_queryset = event.games.all()
        self.to_queryset = Game.objects.exclude(event=event_id)

        self.fields["fromgame"] = forms.ModelChoiceField(
            queryset=self.from_queryset, required=True
        )
        self.fields["togame"] = forms.ModelChoiceField(
            queryset=self.to_queryset, required=True
        )


class GameWishUpdateForm(forms.Form):
    id = forms.IntegerField()
    name = forms.CharField()
    rating = forms.ChoiceField(
        choices=GameWish.rating_choices, help_text="Hur mycket vill du ha med spelet??"
    )


class GameImportanceUpdateForm(forms.Form):
    id1 = forms.IntegerField()
    id2 = forms.IntegerField()
    name1 = forms.CharField()
    name2 = forms.CharField()
    rating = forms.ChoiceField(
        choices=[(-1, "Välj"), (0, "→"), (1, "LIKA"), (2, "←")],
        help_text="Hur mycket vill du ha med spelet??",
    )

    def clean_rating(self):
        data = self.cleaned_data["rating"]
        choice_ids = [x[0] for x in ImportanceRating.importance_choices]

        if not int(data) in choice_ids:
            raise ValidationError("Du måste välja ett alternativ!")

        return data


class GameImportance2UpdateForm(forms.Form):
    id = forms.IntegerField(required=True)
    name = forms.CharField(required=False)
    score = forms.FloatField(
        required=True,
        label=False,
        widget=forms.TextInput(
            attrs={
                "step": "0.1",
                "type": "range",
                "value": "0.5",
                "min": "0.0",
                "max": "1",
            }
        ),
    )


class NewsCreateForm(forms.ModelForm):
    class Meta:
        model = News
        exclude = ("date", "added_by")


class PlayerTokenCreateForm(forms.ModelForm):
    class Meta:
        model = PlayerCreateToken
        fields = ("email",)

    def clean_email(self):
        data = self.cleaned_data["email"]

        if User.objects.filter(username=data).count() > 0:
            raise ValidationError("Användaren finns redan!")

        return data


class FoodCreateUpdateForm(forms.ModelForm):
    class Meta:
        model = Food
        exclude = ("added_by",)


class MoneyTransactionCreateUpdateForm(forms.ModelForm):
    class Meta:
        model = MoneyTransaction
        exclude = ("added_by",)
