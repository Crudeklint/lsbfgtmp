from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

import datetime


class Player(models.Model):
    gamertag = models.CharField(max_length=30, blank=False, null=True, unique=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    get_event_notifications = models.BooleanField(
        "Vill ha event-notifikationer", default=False, blank=False
    )
    get_news_notifications = models.BooleanField(
        "Vill ha nyhetsnotifikationer", default=False, blank=False
    )
    image = models.ImageField(upload_to="profiles", null=True, blank=True)
    verified_mail = models.BooleanField("Verifierad mail", default=False, blank=False)

    @property
    def get_photo_url(self):
        if self.image and hasattr(self.image, "url"):
            return self.image.url
        else:
            return "/static/gamemanager/images/default_user_profile.svg"

    def __str__(self):
        return '%s "%s" %s' % (self.user.first_name, self.gamertag, self.user.last_name)

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        if self.user:
            self.user.delete()


class Team(models.Model):
    teamname = models.CharField(max_length=100, blank=False)
    players = models.ManyToManyField(Player, verbose_name="Spelare", blank=False)


class Rating(models.Model):
    ratings = (
        (0, "Inget betyg"),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (-1, "Vidrigt"),
    )

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")

    player = models.ForeignKey(
        Player,
        verbose_name="Spelare",
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    rating = models.IntegerField("Betyg", choices=ratings, default=0)

    class Meta:
        unique_together = ("content_type", "object_id", "player")


class Food(models.Model):
    name = models.CharField("Maträtt", max_length=200)
    description = models.TextField("Recept", blank=True, null=True)
    recipe_link = models.URLField("Receptlänk", blank=True, null=True)
    added_by = models.ForeignKey(
        Player,
        verbose_name="Tillagt av",
        on_delete=models.CASCADE,
        blank=False,
        default=0,
    )
    image = models.ImageField(upload_to="food_images", null=True, blank=True)

    class Meta:
        ordering = ["name"]

    @property
    def get_photo_url(self):
        if self.image and hasattr(self.image, "url"):
            return self.image.url
        else:
            return "#"

    def __str__(self):
        return self.name


class PlayerCreateToken(models.Model):
    token = models.CharField(
        max_length=30,
        blank=False,
        default="000000000000000000000000000000",
        unique=True,
    )
    email = models.EmailField(blank=False, null=True, unique=True)
    expire_date = models.DateTimeField(
        blank=False, null=True, default=datetime.datetime(2000, 1, 1)
    )

    class Meta:
        ordering = ["-expire_date"]


class News(models.Model):
    heading = models.CharField("Rubrik", max_length=100, blank=False, null=True)
    article_text = models.TextField("Text", blank=False, null=True)
    date = models.DateTimeField("Datum", auto_now_add=True, blank=False, null=True)
    added_by = models.ForeignKey(
        Player,
        verbose_name="Tillagt av",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '"%s"' % (self.heading)

    class Meta:
        ordering = ["-date"]


class Game(models.Model):
    name = models.CharField("Namn", max_length=200)
    system = models.CharField(max_length=100)
    no_of_players = models.IntegerField("Antal spelare", blank=False, null=True)
    boxart = models.ImageField(upload_to="boxarts", null=True, blank=True)
    youtube = models.URLField("Youtube-länk", blank=True, null=True)
    description = models.TextField("Beskrivning", blank=True, null=True)
    added_by = models.ForeignKey(
        Player,
        verbose_name="Tillagt av",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ["system", "name"]

    @property
    def get_photo_url(self):
        if self.boxart and hasattr(self.boxart, "url"):
            return self.boxart.url
        else:
            return "/static/gamemanager/images/default_game_icon.png"

    def __str__(self):
        return self.name + " (" + self.system + ")"


class BoardGame(models.Model):
    name = models.CharField("Namn", max_length=200)
    owner = models.ForeignKey(
        Player,
        verbose_name="Ägare",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="owning_player",
    )
    no_of_players = models.IntegerField("Antal spelare", blank=False, null=True)
    boxart = models.ImageField(upload_to="boardgame_boxarts", null=True, blank=True)
    youtube = models.URLField("Youtube-länk", blank=True, null=True)
    description = models.TextField("Beskrivning", blank=True, null=True)
    added_by = models.ForeignKey(
        Player,
        verbose_name="Tillagt av",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    @property
    def get_photo_url(self):
        if self.boxart and hasattr(self.boxart, "url"):
            return self.boxart.url
        else:
            return "/static/gamemanager/images/default_game_icon.png"


class Event(models.Model):
    statuses_choices = (
        (0, "Pending"),
        (1, "Voting"),
        (2, "Playing"),
        (3, "Finished"),
        (4, "Locked"),
    )

    types_choices = (
        (0, "Game tournament"),
        (1, "General"),
        (2, "Board games"),
    )

    class Meta:
        ordering = ["-date"]

    name = models.CharField("Namn", max_length=200)
    status = models.IntegerField(
        "Status", choices=statuses_choices, blank=False, default=0
    )
    type = models.IntegerField("Typ", choices=types_choices, blank=False, default=1)
    location = models.CharField("Plats", max_length=200, blank=True, null=True)
    location_href = models.URLField("Plats-länk", max_length=200, blank=True, null=True)
    info = models.TextField("Extra information", blank=True, null=True)
    date = models.DateTimeField("Datum")
    date_preliminary = models.BooleanField("Preliminärt datum", default=True)
    games = models.ManyToManyField(Game, verbose_name="Spel", blank=True)
    boardgames = models.ManyToManyField(BoardGame, verbose_name="Brädspel", blank=True)
    players = models.ManyToManyField(Player, verbose_name="Spelare", blank=True)
    cannot_attend = models.ManyToManyField(
        Player,
        verbose_name="Spelare som ej kan vara med",
        blank=True,
        related_name="players_noattend",
    )
    hidden_score = models.BooleanField("Dölj poäng", default=True)
    wallpaper = models.ImageField(upload_to="events", blank=True, default="")
    logo = models.ImageField(upload_to="events", blank=True, default="")
    added_by = models.ForeignKey(
        Player,
        verbose_name="Skapare",
        on_delete=models.CASCADE,
        null=True,
        related_name="added_by",
    )
    score = models.IntegerField("Poäng", blank=False, default=1000000)
    thirdplace_factor = models.FloatField(
        "Tredjeplats poängfaktor", blank=False, default=0.1
    )
    secondplace_factor = models.FloatField(
        "Andraplats poängfaktor", blank=False, default=0.3
    )
    firstplace_factor = models.FloatField(
        "Förstaplats poängfaktor", blank=False, default=0.6
    )
    food = models.ManyToManyField(Food, verbose_name="Maträtter", blank=True)
    bracket_data = models.TextField("Bracket data", blank=True, null=True)

    def __str__(self):
        return self.name


# ---


class TournamentEventManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=0)


class TournamentEvent(Event):
    objects = TournamentEventManager()

    class Meta:
        proxy = True


class GeneralEventManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=1)


class GeneralEvent(Event):
    objects = GeneralEventManager()

    class Meta:
        proxy = True


class BoardGameEventManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=2)


class BoardGameEvent(Event):
    objects = BoardGameEventManager()

    class Meta:
        proxy = True


# ---


class GameWish(models.Model):
    rating_choices = (
        (-1, "Veto"),
        (0, "No"),
        (1, "Yes"),
        (2, "Favourite"),
    )

    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True)
    player = models.ForeignKey(Player, on_delete=models.CASCADE, null=True)
    rating = models.IntegerField(choices=rating_choices, null=True)

    class Meta:
        unique_together = ("event", "player", "game")


class ImportanceRating(models.Model):
    importance_choices = (
        (0, "Less"),
        (1, "Same"),
        (2, "More"),
    )

    game1 = models.ForeignKey(
        Game,
        verbose_name="Spel 1",
        on_delete=models.CASCADE,
        blank=False,
        null=True,
        related_name="game1",
    )
    game2 = models.ForeignKey(
        Game,
        verbose_name="Spel 2",
        on_delete=models.CASCADE,
        blank=False,
        null=True,
        related_name="game2",
    )
    score = models.IntegerField(
        "Viktighet", choices=importance_choices, blank=False, null=True
    )
    event = models.ForeignKey(
        Event, verbose_name="Event", on_delete=models.CASCADE, blank=False, null=True
    )
    player = models.ForeignKey(
        Player, verbose_name="Spelare", on_delete=models.CASCADE, blank=False, null=True
    )

    def __str__(self):
        return "%s: %s: %s vs %s - %s" % (
            self.event,
            self.player,
            self.game1,
            self.game2,
            self.score,
        )


class ImportanceRating2(models.Model):
    game = models.ForeignKey(
        Game,
        verbose_name="Spel 1",
        on_delete=models.CASCADE,
        blank=False,
        null=True,
        related_name="game",
    )
    score = models.FloatField(
        "Viktighet",
        default=0.5,
        validators=[MaxValueValidator(1), MinValueValidator(0)],
        blank=False,
        null=True,
    )
    event = models.ForeignKey(
        Event, verbose_name="Event", on_delete=models.CASCADE, blank=False, null=True
    )
    player = models.ForeignKey(
        Player, verbose_name="Spelare", on_delete=models.CASCADE, blank=False, null=True
    )

    def __str__(self):
        return "%s: %s: %s - %s" % (
            self.event,
            self.player,
            self.game,
            self.score,
        )


class Placement(models.Model):
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        blank=False,
        null=True,
        related_name="placement_event",
    )
    game = models.ForeignKey(Game, on_delete=models.CASCADE, blank=False, null=True)
    thirdplace = models.ManyToManyField(
        Player, verbose_name="Tredjeplats", blank=True, related_name="place3"
    )
    secondplace = models.ManyToManyField(
        Player, verbose_name="Andraplats", blank=True, related_name="place2"
    )
    firstplace = models.ManyToManyField(
        Player, verbose_name="Förstaplats", blank=True, related_name="place1"
    )

    def __str__(self):
        firsts = ", ".join([x["gamertag"] for x in self.firstplace.all().values()])
        seconds = ", ".join([x["gamertag"] for x in self.secondplace.all().values()])
        thirds = ", ".join([x["gamertag"] for x in self.thirdplace.all().values()])

        return "%s: %s - 1: %s, 2: %s, 3: %s" % (
            self.event,
            self.game,
            firsts,
            seconds,
            thirds,
        )


class BugReport(models.Model):
    report = models.TextField("Bugg")
    added_by = models.ForeignKey(
        Player, verbose_name="Rapporterare", on_delete=models.CASCADE
    )
    added_date = models.DateTimeField("Datum", auto_now_add=True, blank=True)


class ScoreRecord(models.Model):
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        blank=False,
        default=0,
        related_name="scorerecord_event",
    )
    player = models.ForeignKey(
        Player, verbose_name="Spelare", on_delete=models.CASCADE, blank=False
    )
    score = models.IntegerField("Poäng", blank=False)

    def __str__(self):
        return "{}: {} - {}".format(self.event, self.player, self.score)

    class Meta:
        unique_together = (
            "event",
            "player",
        )
        ordering = ["-score"]


class ExtraPoint(models.Model):
    points = models.IntegerField("Poäng", default=0, blank=False, null=False)
    description = models.TextField("Motivering", blank=False, null=False)
    player = models.ForeignKey(
        Player,
        verbose_name="Till spelare",
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    from_player = models.ForeignKey(
        Player,
        verbose_name="Från spelare",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="scorerecord_event",
    )
    event = models.ForeignKey(
        Event, verbose_name="Event", on_delete=models.CASCADE, blank=False, null=False
    )

    def __str__(self):
        return "{} poäng {} --> {}".format(self.points, self.from_player, self.player)

    def save(self, *args, **kwargs):
        super(ExtraPoint, self).save(*args, **kwargs)

        ExtraPoint.objects.all().filter(points=0).delete()


class MoneyTransaction(models.Model):
    directions = ((-1, "Från LSBFGT"), (1, "Till LSBFGT"))
    direction = models.IntegerField(
        "Riktning", choices=directions, blank=False, null=False
    )
    amount = models.IntegerField("Summa", blank=False, null=False)
    date = models.DateTimeField("Datum", auto_now_add=True, blank=False, null=False)
    description = models.TextField("Beskrivning", blank=True, null=True)
    player = models.ForeignKey(
        Player,
        verbose_name="spelare",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="player",
    )
