"""lsbfgtmp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
        https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
        1. Add an import:    from my_app import views
        2. Add a URL to urlpatterns:    path('', views.home, name='home')
Class-based views
        1. Add an import:    from other_app.views import Home
        2. Add a URL to urlpatterns:    path('', Home.as_view(), name='home')
Including another URLconf
        1. Import the include() function: from django.urls import include, path
        2. Add a URL to urlpatterns:    path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include


from . import views

# fmt: off
urlpatterns = [
    path("", views.home, name="home"),

    path("events/", views.EventListView.as_view(), name="event list"),
    path("events/search/<str:search>/", views.EventFilteredListView.as_view(), name="event filtered list"),
    path("events/create/", views.EventUpdateCreateView.as_view(), name="event create"),
    path("events/<int:pk>/", views.EventDetailView.as_view(), name="event detail"),
    path("events/<int:event_id>/update_status/<int:status_id>/", views.event_update_status, name="event update status"),
    path("events/<int:pk>/delete/", views.EventDeleteView.as_view(), name="event delete"),
    path("events/<int:pk>/qr/", views.EventQRView.as_view(), name="event qr"),
    path("events/<int:pk>/live/", views.event_live, name="event live"),
    path("events/<int:pk>/live/brackets/set", views.event_set_brackets, name="event live set brackets"),
    path("events/<int:pk>/live/brackets/get", views.event_get_brackets, name="event live get brackets"),
    path("events/<int:pk>/live/admin/", views.event_live_admin, name="event live admin"),
    path("events/<int:pk>/live_content/", views.event_live_content, name="event live content"),
    path("events/<int:pk>/live_extrapoints/", views.live_extrapoints, name="event live extrapoints"),
    path("events/<int:pk>/placements/delete/", views.EventPlacementsDelete.as_view(), name="event delete placements"),
    path("events/<int:event_id>/gamewish/", views.event_gamewish, name="event gamewish"),
    path("events/<int:event_id>/gameimportance/", views.event_gameimportance, name="event gameimportance"),
    path("events/<int:pk>/gameimportance2/", views.event_gameimportance_2, name="event gameimportance2"),
    path("events/<int:event_id>/gameimportance/delete/<int:player_id>/", views.EventGameImportanceDelete.as_view(), name="event gameimportance delete"),
    path("events/<int:event_id>/gameimportance2/delete/<int:player_id>/", views.EventGameImportance2Delete.as_view(), name="event gameimportance2 delete"),
    path("events/<int:event_id>/signdown/", views.event_signdown, name="event signdown"),
    path("events/<int:event_id>/signup/", views.event_signup, name="event signup"),
    path("events/<int:pk>/update/", views.EventUpdateCreateView.as_view(), name="event update"),
    path("events/<int:pk>/insert_scores/", views.score_record_insert, name="event insert-score"),
    path("events/<int:pk>/post_summary/", views.EventPostSummary.as_view(), name="event post summary"),

    path("games/", views.GameListView.as_view(), name="game list"),
    path("games/add/", views.GameCreateUpdateView.as_view(), name="game add"),
    path("games/import/steam/", views.GameImportSteamView.as_view(), name="game import steam"),
    path("games/<int:pk>/", views.GameDetailView.as_view(), name="game detail"),
    path("games/<int:pk>/update/", views.GameCreateUpdateView.as_view(), name="game update"),
    path("games/<int:pk>/delete/", views.GameDeleteView.as_view(), name="game delete"),
    path("games/import/steam/", views.GameImportSteamView.as_view(), name="game import steam"),

    path("boardgames/", views.BoardGameListView.as_view(), name="boardgame list"),
    path("boardgames/add/", views.BoardGameCreateUpdateView.as_view(), name="boardgame add"),
    path("boardgames/<int:pk>/", views.BoardGameDetailView.as_view(), name="boardgame detail"),
    path("boardgames/<int:pk>/update/", views.BoardGameCreateUpdateView.as_view(), name="boardgame update"),
    path("boardgames/<int:pk>/delete/", views.BoardGameDeleteView.as_view(), name="boardgame delete"),
    path("boardgames/import/bgg/", views.BoardGameImportBGGView.as_view(), name="boardgame import bgg"),

    path("score/", views.ScoreRecordListView.as_view(), name="score list"),
    path("score/<int:year>/", views.ScoreRecordListView.as_view(), name="score list"),

    path("accounts/", views.PlayerListView.as_view(), name="player list"),
    path("accounts/adminlist", views.PlayerAdminListView.as_view(), name="player list admin"),
    path("accounts/login/", views.CustomLoginView.as_view(), name="login"),
    path("accounts/logout/", views.custom_logout, name="logout"),
    path("accounts/create/", views.player_create, name="player create"),
    path("accounts/<int:pk>/", views.PlayerDetailView.as_view(), name="player detail"),
    path("accounts/<int:pk>/update/", views.PlayerUpdateView.as_view(), name="player update"),
    path("accounts/<int:pk>/delete/", views.PlayerDeleteView.as_view(), name="player delete"),
    path("accounts/<int:pk>/update_password/", views.PlayerUpdatePasswordView.as_view(), name="player update-password"),
    path("accounts/tokens/", views.PlayerTokenListView.as_view(), name="usertoken list"),
    path("accounts/tokens/create/", views.PlayerTokenCreateView.as_view(), name="usertoken create"),
    path("accounts/tokens/<int:pk>/delete/", views.PlayerTokenDeleteView.as_view(), name="usertoken delete"),
    path("accounts/create/<str:token>/", views.player_create, name="user create with token"),

    path("bugs/", views.BugListView.as_view(), name="bugreport list"),
    path("bugs/create/", views.BugCreateUpdateView.as_view(), name="bugreport create"),
    path("bugs/<int:pk>/update/", views.BugCreateUpdateView.as_view(), name="bugreport update"),
    path("bugs/<int:pk>/delete/", views.BugDeleteView.as_view(), name="bugreport delete"),

    path("news/", views.NewsListView.as_view(), name="news list"),
    path("news/<int:pk>/", views.NewsDetailView.as_view(), name="news detail"),
    path("news/create/", views.NewsUpdateCreateView.as_view(), name="news create"),
    path("news/<int:pk>/update/", views.NewsUpdateCreateView.as_view(), name="news update"),
    path("news/<int:pk>/delete/", views.NewsDeleteView.as_view(), name="news delete"),

    path("food/", views.FoodListView.as_view(), name="food list"),
    path("food/<int:pk>/", views.FoodDetailView.as_view(), name="food detail"),
    path("food/create/", views.FoodUpdateCreateView.as_view(), name="food create"),
    path("food/<int:pk>/update/", views.FoodUpdateCreateView.as_view(), name="food update"),
    path("food/<int:pk>/delete/", views.FoodDeleteView.as_view(), name="food delete"),

    path("moneytransactions/create/", views.MoneyTransactionUpdateCreateView.as_view(), name="moneytransactions create"),
    path("moneytransactions/", views.MoneyTransactionListView.as_view(), name="moneytransactions list"),
    path("moneytransactions/<int:pk>/update/", views.MoneyTransactionUpdateCreateView.as_view(), name="moneytransactions update"), 

    path("commits/", views.commit_list_view, name="commit list"),
    
    path("resources/", views.resources, name="resources"),

    path("rate/<str:model_str>/<int:pk>/<str:rating>/", views.rating_view, name="generic rating"),

    path("extrapoints/<int:pk>/delete/", views.extrapoints_delete, name="extrapoints delete"),
]
# fmt: on
