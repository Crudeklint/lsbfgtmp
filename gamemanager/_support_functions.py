import textwrap
import os
import datetime
import re
import threading
import sib_api_v3_sdk

from .models import ImportanceRating, ImportanceRating2, Placement, Player, ExtraPoint

from django.conf import settings
from django.core.mail import send_mail
from django.core.exceptions import PermissionDenied
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.html import strip_tags
from django.views import generic

from operator import itemgetter

from git import Repo

import itertools


class CreateUpdateView(generic.UpdateView):
    def get_object(self, queryset=None):
        try:
            return super().get_object(queryset)
        except AttributeError:
            return None


class EmailThread(threading.Thread):
    def __init__(self, subject, html_content, recipient_list):
        self.subject = "LSBFGTMP - " + subject
        self.recipient_list = recipient_list
        self.html_content = html_content
        threading.Thread.__init__(self)

    def run(self):
        configuration = sib_api_v3_sdk.Configuration()
        configuration.api_key["api-key"] = settings.BREVO_API_KEY

        api_instance = sib_api_v3_sdk.TransactionalEmailsApi(
            sib_api_v3_sdk.ApiClient(configuration)
        )

        send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(
            to=[{"email": x} for x in self.recipient_list],
            html_content=self.html_content,
            sender={"name": "LSBFGTMP", "email": "c.rudeklint@gmail.com"},
            subject=self.subject,
        )
        api_response = api_instance.send_transac_email(send_smtp_email)


def send_html_mail(subject, html_content, recipient_list):
    if settings.DEBUG:
        recipient_list = [settings.DEBUG_MAIL_RECIPIENT]

    EmailThread(subject, html_content, recipient_list).start()


def send_token_via_mail(token, recipient):
    subject = "Inbjudan"
    href = settings.BASE_URL + str(
        reverse_lazy("user create with token", kwargs={"token": token})
    )

    data = {
        "pre_buttons_message": """
Du har blivit inbjuden till det exklusiva communityt 
Luleå Super Best Friends Game Tournament (LSBFGT).

Klicka på länken för att skapa en användare.
""",
        "post_buttons_message": "Länken är giltig i 24h.",
        "buttons": {
            href: "Jag vill va med",
        },
    }

    msg_html = render_to_string("gamemanager/html_mail_template_2.html", data)

    recipient_list = [recipient]

    send_html_mail(subject, msg_html, recipient_list)


def send_event_notification_via_mail(event):
    subject = "Nytt event"
    href = settings.BASE_URL + str(
        reverse_lazy("event signup", kwargs={"event_id": event.id})
    )
    href2 = settings.BASE_URL + str(
        reverse_lazy("event signdown", kwargs={"event_id": event.id})
    )
    href3 = (
        settings.BASE_URL
        + str(reverse_lazy("event detail", kwargs={"pk": event.id}))
        + "#comments"
    )

    data = {
        "pre_buttons_message": """
Hej!

Det finns nu ett nytt event tillgängligt:
""",
        "event": event,
        "buttons": {
            href: "Jag vill va med",
            href2: "Jag vill inte va med",
            href3: "Skriv en kommeentar",
        },
    }

    msg_html = render_to_string("gamemanager/html_mail_template_2.html", data)

    notification_query = (
        Player.objects.filter(get_event_notifications=True)
        .filter(verified_mail=True)
        .values("get_event_notifications", "user__username")
    )

    recipient_list = [x["user__username"] for x in notification_query]

    send_html_mail(subject, msg_html, recipient_list)


def send_signup_notification_via_mail(event, player, signdown=False):
    event_str = str(event)
    player_str = str(player)

    event_owner = event.added_by

    if event_owner.get_event_notifications == False:
        return

    yesno = "tackat nej" if (signdown) else "tackat ja"

    subject = "Användare har " + yesno

    data = {
        "pre_buttons_message": """
Hej!

{} 

har {} till ditt event:
""".format(
            player_str, yesno
        ),
        "event": event,
    }

    msg_html = render_to_string("gamemanager/html_mail_template_2.html", data)

    recipient_list = [event_owner.user.username]

    send_html_mail(subject, msg_html, recipient_list)


def send_postgame_summary_via_mail(event):
    href = settings.BASE_URL + str(
        reverse("event post summary", kwargs={"pk": event.id})
    )

    notification_query = (
        Player.objects.filter(get_event_notifications=True)
        .filter(verified_mail=True)
        .values("get_event_notifications", "user__username")
    )

    recipient_list = [x["user__username"] for x in notification_query]

    subject = "Din spelsummering"

    data = {
        "pre_buttons_message": """
Tack för att du var med och spelade!

Klicka på knappen nedan för att se din summering.
""",
        "buttons": {
            href: "Min summering",
        },
        "post_buttons_message": "Kom ihåg att om man inte röstar är man en vegan leftist soyboybetacuck.",
    }

    msg_html = render_to_string("gamemanager/html_mail_template_2.html", data)

    send_html_mail(subject, msg_html, recipient_list)


def send_news_via_mail(news):
    href = settings.BASE_URL + str(reverse("news detail", kwargs={"pk": news.id}))

    notification_query = (
        Player.objects.filter(get_news_notifications=True)
        .filter(verified_mail=True)
        .values("get_news_notifications", "user__username")
    )

    recipient_list = [x["user__username"] for x in notification_query]

    subject = "Nyheter på LSBFGTMP"

    data = {
        "pre_buttons_message": "Det finns en ny nyhet på LSBFGTMP.",
        "buttons": {
            href: "Läs här",
        },
    }

    msg_html = render_to_string("gamemanager/html_mail_template_2.html", data)

    send_html_mail(subject, msg_html, recipient_list)


def parse_conventional_commit(commitmess):
    commit_info_arr = re.findall("(?:^|\n)(\w+): ([\s\S]+?)(?=(?:\n\w)|$)", commitmess)

    parsed_message = ""

    replaces = {
        "feat": "Features",
        "fix": "Fixes",
        "chore": "Chores",
        "refactor": "Code refactorings",
        "docs": "Documentation",
        "build": "Build script",
        "style": "Style",
        "perf": "Performance enhancements",
        "test": "Testing",
    }

    types = []
    for i in range(0, len(commit_info_arr)):
        type = commit_info_arr[i][0]

        if type in types:
            continue

        types.append(type)

    types.sort()

    for type in types:
        prettytype = replaces[type] if (type in replaces) else type.capitalize()
        parsed_message += prettytype + ":\n\n"

        for mess in [line[1] for line in commit_info_arr if (line[0] == type)]:
            mess_no_nl = mess.replace("\r", "").replace("\n", "_")
            mess_no_indent = re.sub(" {2,}", " ", mess_no_nl)

            mess_wrapped = textwrap.wrap(mess_no_indent, 80)
            mess_real_indent = "- " + mess_wrapped[0] + "\n  ".join(mess_wrapped[1:])

            parsed_message += mess_real_indent + "\n"

        parsed_message += "\n\n"

    return parsed_message


def parse_git_log(limit=None):
    git_dir = os.path.join(settings.BASE_DIR, ".git")
    repo = Repo(git_dir)

    returnvalue = []

    commits = list(repo.iter_commits("master"))
    for i in range(0, len(commits)):
        if limit and i == limit:
            break

        commit = commits[i]
        unixtime = int(commit.committed_date)

        date = datetime.datetime.utcfromtimestamp(unixtime).strftime(
            "%Y-%m-%d %H:%M:%S"
        )
        parsed_message = parse_conventional_commit(commit.message)

        if parsed_message == "":
            parsed_message = commit.message

        returnvalue.append({"date": date, "message": parsed_message})

    return returnvalue


def admin_check(user):
    return user.groups.filter(name__in=["admins"])


def admin_or_creator_check(user, obj):
    if not (admin_check(user) or obj.added_by == user.player):
        raise (PermissionDenied)


def create_importance_pairs(event):
    games = [x.id for x in event.games.all()]
    asd = []

    for a in list(itertools.permutations(games, 2)):
        b = sorted(list(a))
        b.sort()

        if not b in asd:
            asd.append(b)

    asd.sort(key=itemgetter(0, 1))

    return asd


def delete_old_tokens():
    PlayerCreateToken.objects.filter(expire_date__lte=datetime.date.today())


def validate_importance_votes(event):
    statuses = {
        "status": {
            "voted_all_old": [],
            "voted_none_old": [],
            "voted_some_old": [],
            "voted_all_new": [],
            "voted_none_new": [],
            "voted_some_new": [],
        },
        "missing": {},
    }

    statuses2 = {}

    gameids = list(event.games.values_list("id", flat=True))

    correct_importance_pairs = create_importance_pairs(event)
    correct_length = len(correct_importance_pairs)
    players = [x for x in event.players.all()]

    for player in players:
        # ==========
        # OLD VOTING
        # ==========
        player_votes = ImportanceRating.objects.filter(
            event=event, player=player
        ).values()

        sorted_tuples = [[a["game1_id"], a["game2_id"]] for a in player_votes]
        sorted_tuples.sort(key=itemgetter(0, 1))

        statuses["missing"][player] = []

        missing_pairs = 0
        missing_tuples = []

        for tuple in correct_importance_pairs:
            if not tuple in sorted_tuples:
                missing_pairs += 1
                statuses["missing"][player].append(tuple)
                missing_tuples.append(tuple)

        if missing_pairs == 0:
            statuses["status"]["voted_all_old"].append({player: []})
            thisstat_old = []
        elif missing_pairs == correct_length:
            statuses["status"]["voted_none_old"].append({player: []})
            thisstat_old = None
        else:
            statuses["status"]["voted_some_old"].append({player: missing_tuples})
            thisstat_old = missing_tuples

        # ==========
        # NEW VOTING
        # ==========

        player_votes2 = list(
            ImportanceRating2.objects.filter(event=event, player=player).values_list(
                "game", flat=True
            )
        )

        matches = [x for x in gameids if x in player_votes2]
        missing_ids = [x for x in gameids if x not in player_votes2]

        if len(missing_ids) == 0:
            statuses["status"]["voted_all_new"].append({player: []})
            thisstat_new = []
        elif len(missing_ids) == len(gameids):
            statuses["status"]["voted_none_new"].append({player: []})
            thisstat_new = None
        else:
            statuses["status"]["voted_some_new"].append({player: missing_ids})
            thisstat_new = missing_ids

        statuses2[player] = {}
        statuses2[player]["old"] = missing_tuples
        statuses2[player]["new"] = missing_ids

    return statuses2


def calculate_importances(event):
    event_games = event.games.all()

    # ==========
    # NEW SCORES
    # ==========

    importance_ratings2 = (
        ImportanceRating2.objects.all()
        .filter(event=event, game__in=event_games)
        .values()
    )

    ratings = {
        k: list(g)
        for k, g in itertools.groupby(importance_ratings2, key=lambda q: q["player_id"])
    }
    new_player_ratings = {}

    for player_id in ratings:
        scores = [x["score"] + 1 for x in ratings[player_id]]

        sumval = sum(scores)
        scaled_scores = {
            x["game_id"]: (x["score"] + 1) / sumval for x in ratings[player_id]
        }

        new_player_ratings[player_id] = scaled_scores

    # ==========
    # OLD SCORES
    # ==========

    score = {}
    ratings = {}
    game_ids = []

    importance_ratings = ImportanceRating.objects.all().filter(
        event=event, game1__in=event_games, game2__in=event_games
    )

    for game in event.games.all():
        game_ids.append(game.id)

    for game_id1 in game_ids:
        ratings[game_id1] = {}
        for game_id2 in game_ids:
            ratings[game_id1][game_id2] = []

    for existing_importance in importance_ratings:
        game_id1 = existing_importance.game1.id
        game_id2 = existing_importance.game2.id

        rating = existing_importance.score

        player_id = existing_importance.player.id

        if player_id in new_player_ratings:
            continue

        try:
            ratings[game_id1][game_id2].append(rating)
            ratings[game_id2][game_id1].append(abs(rating - 2))
        except KeyError:
            continue

    for game_id1 in ratings:
        ratings[game_id1][game_id1] = [1]

        for game_id2 in ratings[game_id1]:
            ratings[game_id1][game_id2] = sum(ratings[game_id1][game_id2])

    score = {}

    for game_id1 in ratings:
        score[game_id1] = sum([x[1] for x in ratings[game_id1].items()])

    total_sum = sum(score.values())

    for game_id1 in ratings:
        score[game_id1] = score[game_id1] / total_sum

    # Add scores for new vote type
    for player_id in new_player_ratings:
        for game_id in new_player_ratings[player_id]:
            score[game_id] += new_player_ratings[player_id][game_id]

    total_sum = sum(score.values())

    for game_id1 in ratings:
        score[game_id1] = score[game_id1] / total_sum

    return score


def calculate_scores(event, score_weights):
    players = event.players.all()
    event_games = event.games.all()
    places = Placement.objects.all().filter(event=event, game__in=event_games)
    extra_points = ExtraPoint.objects.all().filter(event=event)

    points = {
        "firstplace_id": event.firstplace_factor,
        "secondplace_id": event.secondplace_factor,
        "thirdplace_id": event.thirdplace_factor,
    }

    playerscore = {}

    for player in players:
        playerscore[player.id] = 0

    for place in places:
        for field in ["firstplace", "secondplace", "thirdplace"]:
            for player in getattr(place, field).all():
                playerid = player.id
                gameid = place.game.id

                if not playerid in playerscore:
                    continue

                if not playerid:
                    continue

                if not gameid in score_weights:
                    continue

                playerscore[playerid] += (
                    points[field + "_id"] * event.score * score_weights[gameid]
                )

    for extra_point in extra_points:
        playerscore[extra_point.player.id] += extra_point.points

    playerscore = sorted(playerscore.items(), key=lambda x: x[1], reverse=True)

    return playerscore


def migrate_old_rankings():
    from .models import GameRating, Rating
    from django.contrib.contenttypes.models import ContentType

    old_ratings = GameRating.objects.all()
    for rating in old_ratings:
        player = rating.player
        pk = rating.game.id
        content_type = ContentType.objects.get_for_model(rating.game)
        rating = rating.rating

        try:
            rating_req = Rating.objects.get(
                content_type=content_type, object_id=pk, player=player
            )

            if rating == 0:
                delete = True
            else:
                rating_req.rating = rating

        except Rating.DoesNotExist:
            rating_req = Rating(
                content_type=content_type, object_id=pk, rating=rating, player=player
            )

        rating_req.save()


def generate_base64_qr(href):
    img = qrcode.make(href, image_factory=PyPNGImage)

    buffered = BytesIO()
    img.save(buffered)
    img_str = base64.b64encode(buffered.getvalue())

    img_base64 = "data:image/jpeg;base64," + img_str.decode("utf-8")

    return img_base64


def replace_game_in_event(event, fromgame, togame):
    all_ratings = ImportanceRating.objects.filter(event=event)

    for rating in all_ratings:
        if rating.game1 == fromgame:
            if togame.id > rating.game2.id:
                rating.game1 = rating.game2
                rating.game2 = togame
            else:
                rating.game1 = togame
            rating.save()
        elif rating.game2 == fromgame:
            if togame.id < rating.game1.id:
                rating.game2 = rating.game1
                rating.game1 = togame
            else:
                rating.game2 = togame
            rating.save()

    event.games.remove(fromgame)
    event.games.add(togame)
    event.save()
