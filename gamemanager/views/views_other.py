import datetime

from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect, get_object_or_404
from django_comments.models import Comment

from ..models import Event, News, Rating, MoneyTransaction

from .._support_functions import send_html_mail, parse_git_log
from django.template.loader import render_to_string

from django.urls import reverse, reverse_lazy


@login_required
def home(request):
    player = request.user.player
    time_fudge = datetime.timedelta(hours=6)
    now = datetime.datetime.now()
    today = datetime.date.today()

    events = Event.objects.all().filter(date__gte=today)
    news = News.objects.all()[:4]

    event_cantidates = (
        Event.objects.all().filter(date__gte=today, players=player).order_by("-date")
    )

    ongoing_events = [
        event
        for event in event_cantidates
        if (event.date < now and event.date + time_fudge > now)
    ]

    try:
        ongoing_event = ongoing_events[0]
    except IndexError:
        ongoing_event = None

    comments = (
        Comment.objects.all().filter(is_removed=False).order_by("-submit_date")[:5]
    )
    ratings = Rating.objects.all().order_by("-pk")[:10]

    return render(
        request,
        "gamemanager/home.html",
        {
            "events": events,
            "comments": comments,
            "ratings": ratings,
            "news": news,
            "ongoing_event": ongoing_event,
        },
    )


@login_required
def commit_list_view(request):
    history = parse_git_log(12)

    return render(request, "gamemanager/commit_list.html", {"history": history})


@login_required
def resources(request):
    lsbfgt_events = Event.objects.filter(name__startswith="lsbfgt")

    return render(
        request, "gamemanager/resources.html", {"lsbfgt_events": lsbfgt_events}
    )


@login_required
def test(request):
    event = Event.objects.get(id=18)
    event_games = event.games.all()

    player = request.user.player

    from ..models import ImportanceRating2
    import itertools
    from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
    import pprint
    import html
    from django.db.models import Max

    importance_ratings2 = (
        ImportanceRating2.objects.all()
        .filter(event=event, game__in=event_games)
        .values()
    )

    ratings = {
        k: list(g)
        for k, g in itertools.groupby(importance_ratings2, key=lambda q: q["player_id"])
    }

    # outputstr = ""

    for player_id in ratings:
        scores = [x["score"] + 1 for x in ratings[player_id]]

        sumval = sum(scores)
        scaled_scores = [x / sumval for x in scores]

        print(scaled_scores)
        print(scores)

        # print(ratings[player].score)
        # print(max)
        # for rating in ratings[player]:
        # print(rating)
        # outputstr += str(rating) + "\n"

    return HttpResponse(html.escape(""))
