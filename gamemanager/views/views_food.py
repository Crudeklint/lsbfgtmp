from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic

from ..models import Food, Player
from .._support_functions import CreateUpdateView, admin_or_creator_check
from ..forms import FoodCreateUpdateForm


class FoodUpdateCreateView(CreateUpdateView):
    model = Food
    form_class = FoodCreateUpdateForm
    success_url = reverse_lazy("food list")
    create_message = "Receptet {} har lagts till!"
    update_message = "Receptet {} har uppdaterats!"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        player = Player.objects.get(user=self.request.user)

        obj = form.save(commit=False)

        pk = obj.pk

        obj.added_by = player
        obj.save()

        if not pk:
            messages.success(self.request, self.create_message.format(obj))
        else:
            messages.success(self.request, self.update_message.format(obj))

        return super().form_valid(form)


class FoodDeleteView(generic.DeleteView):
    model = Food
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("food list")
    delete_message = "{} har tagits bort!"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        user = self.request.user
        obj = self.get_object()

        admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.delete_message.format(self.get_object()))
        return super().delete(request, *args, **kwargs)


@method_decorator(login_required, name="dispatch")
class FoodListView(generic.ListView):
    model = Food


@method_decorator(login_required, name="dispatch")
class FoodDetailView(generic.DetailView):
    model = Food
