from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms import formset_factory
from django.forms.models import modelformset_factory
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import urlparse, unquote
from operator import itemgetter

import itertools

from ..models import (
    Event,
    Player,
    Placement,
    GameWish,
    Game,
    ImportanceRating,
    ExtraPoint,
    ImportanceRating2,
)
from .._support_functions import CreateUpdateView, admin_check, admin_or_creator_check
from .._support_functions import (
    send_event_notification_via_mail,
    send_signup_notification_via_mail,
    send_postgame_summary_via_mail,
)
from .._support_functions import (
    replace_game_in_event,
    validate_importance_votes,
    calculate_importances,
)
from ..forms import (
    EventUpdateForm,
    PlacementForm,
    ExtraPointForm,
    ClientExtraPointForm,
    GameImportanceUpdateForm,
    GameWishUpdateForm,
)
from ..forms import GameImportance2UpdateForm, GameSwapForm


@method_decorator(login_required, name="dispatch")
class EventListView(generic.ListView):
    model = Event
    template_name = "gamemanager/event_list.html"


@method_decorator(login_required, name="dispatch")
class EventFilteredListView(generic.ListView):
    model = Event
    template_name = "gamemanager/event_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not "search" in self.kwargs:
            return context

        event_name = unquote(self.kwargs["search"])
        event = Event.objects.filter(name__startswith=event_name)

        context["object_list"] = event
        context["dynamic_header"] = event_name

        return context


@method_decorator(login_required, name="dispatch")
class EventDetailView(generic.DetailView):
    model = Event
    template_name = "gamemanager/event_detail.html"


class EventQRView(generic.DetailView):
    model = Event
    template_name = "gamemanager/event_qr.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        context["qr_href"] = settings.BASE_URL + reverse(
            "event live", kwargs={"pk": pk}
        )

        return context


class EventDeleteView(generic.DeleteView):
    model = Event
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("event list")
    success_message = "{} har tagits bort."

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()

        messages.success(self.request, self.success_message.format(obj))
        return super().delete(request, *args, **kwargs)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)


class EventUpdateCreateView(CreateUpdateView):
    model = Event
    form_class = EventUpdateForm
    success_url = "/events/"
    create_message = "Eventet {} har lagts till!"
    update_message = "Eventet {} har uppdaterats!"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        obj = form.save(commit=False)

        pk = obj.pk
        name = obj.name

        if not pk:
            obj.added_by = self.request.user.player

        obj.save()

        if not pk:
            send_event_notification_via_mail(obj)
            messages.success(self.request, self.create_message.format(name))
        else:
            messages.success(self.request, self.update_message.format(name))

        return super().form_valid(form, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if not "pk" in self.kwargs:
            return context

        event_id = self.kwargs["pk"]
        event = Event.objects.get(id=event_id)

        gamewishes = GameWish.objects.all().filter(event=event)
        score_list = {}

        for game in Game.objects.all():
            score_list[game.id] = []

        for wish in gamewishes:
            score_list[wish.game.id].append(wish.rating)

        for game_id in score_list:
            no_elems = len(score_list[game_id])
            if no_elems == 0:
                score_list[game_id] = 0
            else:
                score_list[game_id] = sum(score_list[game_id])


        calculate_importances(event)
        context["score_list"] = score_list

        return context


@method_decorator(login_required, name="dispatch")
class EventPostSummary(generic.DetailView):
    model = Event
    template_name = "gamemanager/event_post_summary.html"


def event_live_content(request, pk):
    event = get_object_or_404(Event, pk=pk)
    weights = calculate_importances(event)
    scores = calculate_scores(event, weights)
    id_2_player = {x.pk: x for x in event.players.all()}
    id_2_game = {x.pk: str(x) for x in event.games.all()}
    places = Placement.objects.all().filter(event=event)

    finished_games = [x.game_id for x in places if len(x.firstplace.all()) > 0]

    context = {
        "event": event,
        "id_2_player": id_2_player,
        "id_2_game": id_2_game,
        "scores": scores,
        "finished_games": finished_games,
    }

    return render(request, "gamemanager/event_live_content.html", context)


# TEMPLATE-LESS VIEWS


@csrf_exempt
@login_required
def event_set_brackets(request, pk):
    event = Event.objects.get(id=pk)
    player = request.user.player

    admin_or_creator_check(request.user, event)

    if request.is_ajax():
        if request.method == "POST":
            event.bracket_data = request.body.decode("utf-8")
            event.save()
            return JsonResponse({"status": "Todo added!"})

        return JsonResponse({"status": "FAILFAILFAIL!"})
    else:
        return HttpResponseBadRequest("Invalid request")


@login_required
def event_get_brackets(request, pk):
    event = Event.objects.get(id=pk)
    player = request.user.player

    bracket_data = event.bracket_data

    return HttpResponse(bracket_data, content_type="application/json")


@login_required
def event_update_status(request, event_id, status_id):
    event = Event.objects.get(id=event_id)
    player = request.user.player

    admin_or_creator_check(request.user, event)

    if status_id in [x[0] for x in event.statuses_choices]:
        event.status = status_id
        event.save()

    messages.add_message(request, messages.SUCCESS, "Status ändrad")

    if event.status == 3:
        messages.add_message(request, messages.SUCCESS, "Summeringar utmailade")
        send_postgame_summary_via_mail(event)
        return redirect(reverse("event insert-score", kwargs={"pk": event_id}))
    else:
        return redirect(request.META.get("HTTP_REFERER"))


@login_required
def event_signup(request, event_id):
    player = request.user.player
    event = Event.objects.get(id=event_id)

    event.players.add(player)
    event.cannot_attend.remove(player)

    messages.add_message(
        request, messages.SUCCESS, "Du har registrerat dig på " + event.name
    )

    next_url = request.GET.get("next")

    send_signup_notification_via_mail(event, player)

    if next_url:
        return redirect("%s" % (next_url))
    else:
        return redirect(reverse("event list"))


@login_required
def event_signdown(request, event_id):
    player = request.user.player
    event = Event.objects.get(id=event_id)

    event.players.remove(player)
    event.cannot_attend.add(player)

    messages.add_message(
        request, messages.SUCCESS, "Du har avregistrerat dig från " + event.name
    )

    next_url = request.GET.get("next")

    send_signup_notification_via_mail(event, player, True)

    if next_url:
        return redirect("%s" % (next_url))
    else:
        return redirect(reverse("event list"))


class EventGameImportanceDelete(generic.DeleteView):
    model = ImportanceRating
    template_name = "gamemanager/delete_confim.html"
    success_message = "Vikt-röstningarna har tagits bort."

    def get_object(self, queryset=None):
        event_id = self.kwargs["event_id"]
        player_id = self.kwargs["player_id"]

        event = get_object_or_404(Event, pk=event_id)
        player = get_object_or_404(Player, pk=player_id)

        context = 'Spelvikter av {} i event "{}"'.format(player, event.name)
        return context

    def delete(self, request, *args, **kwargs):
        event_id = self.kwargs["event_id"]
        player_id = self.kwargs["player_id"]

        event = get_object_or_404(Event, pk=event_id)
        player = get_object_or_404(Player, pk=player_id)

        if not event.added_by == request.user.player:
            raise PermissionDenied

        messages.success(self.request, self.success_message)

        queryset = ImportanceRating.objects.filter(event=event, player=player)
        queryset.delete()

        return HttpResponseRedirect(
            reverse("event live admin", kwargs={"pk": event_id})
        )


class EventGameImportance2Delete(generic.DeleteView):
    model = ImportanceRating2
    template_name = "gamemanager/delete_confim.html"
    success_message = "Vikt-röstningarna har tagits bort."

    def get_object(self, queryset=None):
        event_id = self.kwargs["event_id"]
        player_id = self.kwargs["player_id"]

        event = get_object_or_404(Event, pk=event_id)
        player = get_object_or_404(Player, pk=player_id)

        context = 'Spelvikter av {} i event "{}"'.format(player, event.name)
        return context

    def delete(self, request, *args, **kwargs):
        event_id = self.kwargs["event_id"]
        player_id = self.kwargs["player_id"]

        event = get_object_or_404(Event, pk=event_id)
        player = get_object_or_404(Player, pk=player_id)

        if not event.added_by == request.user.player:
            raise PermissionDenied

        messages.success(self.request, self.success_message)

        queryset = ImportanceRating2.objects.filter(event=event, player=player)
        queryset.delete()

        return HttpResponseRedirect(
            reverse("event live admin", kwargs={"pk": event_id})
        )


class EventPlacementsDelete(generic.DeleteView):
    model = Placement
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("event list")

    success_message = "Placeringarna har tagits bort."

    def get_object(self, queryset=None):
        event_id = self.kwargs["pk"]
        event = get_object_or_404(Event, pk=event_id)

        context = 'Placeringar för event "{}"'.format(event.name)
        return context

    def delete(self, request, *args, **kwargs):
        event_id = self.kwargs["pk"]
        event = get_object_or_404(Event, pk=event_id)

        messages.success(self.request, self.success_message)

        queryset = Placement.objects.filter(event=event)
        queryset.delete()

        return HttpResponseRedirect(
            reverse("event live admin", kwargs={"pk": event_id})
        )


# COMPLEX VIEWS


@login_required
def event_gamewish(request, event_id):
    player = request.user.player
    event = Event.objects.get(id=event_id)

    if event.status != 0:
        raise PermissionDenied

    previous_wishes = {}
    initial = []

    existing_gamewishes = GameWish.objects.all().filter(player=player, event=event)

    for existing_wish in existing_gamewishes:
        previous_wishes[existing_wish.game.id] = existing_wish.rating

    for game in Game.objects.all():
        if game.id in previous_wishes:
            rating = previous_wishes[game.id]
        else:
            rating = 0

        initial.append({"id": game.id, "name": game.name, "rating": rating})

    GameWishUpdateFormSet = formset_factory(GameWishUpdateForm, extra=0)
    formset = GameWishUpdateFormSet(initial=initial)
    event = Event.objects.get(id=event_id)

    if request.method == "POST":
        formset = GameWishUpdateFormSet(request.POST)

        for form in formset:
            form.fields["name"].required = False

        if formset.is_valid():
            for form in formset:
                game = Game.objects.get(id=form.cleaned_data["id"])

                try:
                    wish = GameWish.objects.get(event=event, game=game, player=player)
                except GameWish.DoesNotExist:
                    wish = GameWish()

                wish.game = game
                wish.event = event
                wish.player = player
                wish.rating = form.cleaned_data["rating"]
                wish.save()

            messages.add_message(
                request, messages.SUCCESS, "Dina önskemål är tillagda!"
            )

            next_url = request.GET.get("next")
            if next_url:
                return redirect(next_url)
            else:
                return redirect(reverse("home"))

    context = {
        "event": event,
        "formset": formset,
    }

    return render(request, "gamemanager/event_gamewish.html", context)


@login_required
def event_gameimportance_2(request, pk):
    event = get_object_or_404(Event, id=pk)
    player = request.user.player

    if event.status != 1:
        raise PermissionDenied

    games = event.games.all()
    initial = []

    existing_importances = ImportanceRating2.objects.all().filter(
        player=player, event=event
    )

    previous_ratings = {}
    for existing_importance in existing_importances:
        id = existing_importance.game.id
        score = existing_importance.score

        if not id in previous_ratings:
            previous_ratings[id] = score

        previous_ratings[id] = score

    for game in games:
        score = 0.5

        if game.id in previous_ratings:
            score = previous_ratings[game.id]

        initial.append(
            {
                "id": game.id,
                "name": game.name,
                "score": score,
            }
        )

    GameImportanceUpdateFormSet = formset_factory(GameImportance2UpdateForm, extra=0)
    formset = GameImportanceUpdateFormSet(initial=initial)

    if request.method == "POST":
        formset = GameImportanceUpdateFormSet(request.POST)

        if formset.is_valid():
            for form in formset:
                game = Game.objects.get(id=form.cleaned_data["id"])

                try:
                    importance = ImportanceRating2.objects.get(
                        event=event, game=game, player=player
                    )
                except ImportanceRating2.DoesNotExist:
                    importance = ImportanceRating2()

                importance.game = game
                importance.score = form.cleaned_data["score"]
                importance.player = player
                importance.event = event
                importance.save()

            messages.add_message(
                request, messages.SUCCESS, "Dina önskemål är tillagda!"
            )

            next_url = request.GET.get("next")
            if next_url:
                return redirect(next_url)
            else:
                return redirect(reverse("home"))

    context = {"event": event, "formset": formset}

    return render(request, "gamemanager/event_gameimportance2.html", context)


@login_required
def event_gameimportance(request, event_id):
    player = request.user.player
    event = Event.objects.get(id=event_id)

    if event.status != 1:
        raise (PermissionDenied)

    games = list(event.games.all())
    game_names = {}
    game_objs = {}
    game_ids = []
    initial = []
    asd = []

    previous_ratings = {}
    existing_importances = ImportanceRating.objects.all().filter(
        player=player, event=event
    )

    for existing_importance in existing_importances:
        id1 = existing_importance.game1.id
        id2 = existing_importance.game2.id
        rating = existing_importance.score

        if not id1 in previous_ratings:
            previous_ratings[id1] = {}

        previous_ratings[id1][id2] = rating

    for game in games:
        game_ids.append(game.id)
        game_names[game.id] = game.name
        game_objs[game.id] = game

    for a in list(itertools.permutations(game_ids, 2)):
        b = sorted(list(a))
        b.sort()

        if not b in asd:
            asd.append(b)

    asd.sort(key=itemgetter(0, 1))

    for id1, id2 in asd:
        rating = -1

        if id1 in previous_ratings and id2 in previous_ratings[id1]:
            rating = previous_ratings[id1][id2]

        initial.append(
            {
                "id1": id1,
                "name1": game_names[id1],
                "id2": id2,
                "name2": game_names[id2],
                "rating": rating,
            }
        )

    GameImportanceUpdateFormSet = formset_factory(GameImportanceUpdateForm, extra=0)
    formset = GameImportanceUpdateFormSet(initial=initial)

    if request.method == "POST":
        formset = GameImportanceUpdateFormSet(request.POST)

        for form in formset:
            form.fields["name1"].required = False
            form.fields["name2"].required = False

        if formset.is_valid():
            for form in formset:
                game1 = game_objs[form.cleaned_data["id1"]]
                game2 = game_objs[form.cleaned_data["id2"]]
                rating = form.cleaned_data["rating"]

                try:
                    importance = ImportanceRating.objects.get(
                        event=event, game1=game1, game2=game2, player=player
                    )
                except ImportanceRating.DoesNotExist:
                    importance = ImportanceRating()

                importance.game1 = game1
                importance.game2 = game2
                importance.event = event
                importance.player = player
                importance.score = rating

                importance.save()

            messages.add_message(request, messages.SUCCESS, "Röstning införd!")

            next_url = request.GET.get("next")

            if next_url:
                return redirect(next_url)
            else:
                return redirect(request.META.get("HTTP_REFERER"))

    context = {"event": event, "formset": formset}

    return render(request, "gamemanager/event_gameimportance.html", context)


@login_required
def event_live_admin(request, pk):
    event = get_object_or_404(Event, id=pk)

    admin_or_creator_check(request.user, event)

    id_2_player = {}
    id_2_game = {}

    initial_placement = []

    players = event.players.all()
    all_games = Game.objects.all()
    event_games = event.games.all()

    for player in players:
        id_2_player[player.id] = str(player)

    weights = calculate_importances(event)
    weight_validation = validate_importance_votes(event)
    playerscore = calculate_scores(event, weights)

    for game in all_games:
        id_2_game[game.id] = game.name

    for game in event_games:
        existing_placement = Placement.objects.filter(event=event, game=game).first()

        if existing_placement:
            continue

        initial_placement.append(
            {
                "event": event.id,
                "game": game.id,
                "firstplace": None,
                "secondplace": None,
                "thirdplace": None,
            }
        )

    # ---

    queryset_placements = Placement.objects.filter(event=event, game__in=event_games)

    PlacementFormSet = modelformset_factory(
        Placement,
        form=PlacementForm,
        extra=event_games.count() - queryset_placements.count(),
    )
    formset_placement = PlacementFormSet(
        queryset=queryset_placements,
        initial=initial_placement,
        form_kwargs={"event_id": event.id},
    )

    # ---

    queryset_extrapoints = ExtraPoint.objects.filter(event=event)

    ExtraPointFormSet = modelformset_factory(ExtraPoint, form=ExtraPointForm)
    formset_extrapoints = ExtraPointFormSet(
        queryset=queryset_extrapoints,
        initial=[{"event": event.id}],
        form_kwargs={"event_id": event.id},
    )

    # ---

    swap_form = GameSwapForm(event_id=pk)

    context = {
        "event": event,
        "weights": weights,
        "weight_validation": weight_validation,
        "formset_placements": formset_placement,
        "formset_extrapoints": formset_extrapoints,
        "swap_form": swap_form,
        "playerscore": playerscore,
        "id_2_player": id_2_player,
        "statuses": Event.statuses_choices,
        "id_2_game": id_2_game,
        "correct_tuples_count": sum([x for x in range(0, event_games.count())]),
    }

    if request.method == "POST":
        if "placement_submit" in request.POST:
            formset_placement = PlacementFormSet(
                request.POST,
                request.FILES,
                form_kwargs={"event_id": event.id},
                queryset=queryset_placements,
            )

            context["formset_placements"] = formset_placement

            if not formset_placement.is_valid():
                return render(request, "gamemanager/event_live_admin.html", context)
            else:
                formset_placement.save()
                nonexisting_placements = Placement.objects.filter(event=event).exclude(
                    game__in=event_games
                )
                nonexisting_placements.delete()

                return redirect(
                    reverse_lazy("event live admin", kwargs={"pk": event.id})
                )
        elif "extrapoint_submit" in request.POST:
            formset_extrapoints = ExtraPointFormSet(
                request.POST,
                request.FILES,
                queryset=queryset_extrapoints,
                form_kwargs={"event_id": event.id},
            )

            context["formset_extrapoints"] = formset_extrapoints

            if not formset_extrapoints.is_valid():
                return render(request, "gamemanager/event_live_admin.html", context)
            else:
                formset_extrapoints.save()

                return redirect(
                    reverse_lazy("event live admin", kwargs={"pk": event.id})
                )
        elif "swapgame_submit" in request.POST:
            swap_form = GameSwapForm(request.POST, event_id=pk)
            context["swap_form"] = swap_form

            if not swap_form.is_valid():
                return render(request, "gamemanager/event_live_admin.html", context)
            else:
                fromgame = swap_form.cleaned_data["fromgame"]
                togame = swap_form.cleaned_data["togame"]

                replace_game_in_event(event, fromgame, togame)

                return redirect(
                    reverse_lazy("event live admin", kwargs={"pk": event.id})
                )

    return render(request, "gamemanager/event_live_admin.html", context)


def event_live(request, pk):
    if not request.user.is_authenticated:
        login_with_next = (
            reverse("login") + "?next=" + reverse("event live", kwargs={"pk": pk})
        )

        return HttpResponseRedirect(login_with_next)

    event = Event.objects.get(pk=pk)
    current_player = Player.objects.get(pk=request.user.player.id)

    queryset_extrapoints = ExtraPoint.objects.filter(
        event=event, from_player=request.user.player
    )

    form = ClientExtraPointForm(
        event_id=event.id, from_player_id=request.user.player.id
    )

    context = {
        "event": event,
        "content_address": reverse("event live content", kwargs={"pk": pk}),
        "my_points": queryset_extrapoints,
        "content_address2": reverse("event live extrapoints", kwargs={"pk": pk}),
        "form": form,
        "page": "#TournamentInfo",
    }

    if request.method == "POST":
        form = ClientExtraPointForm(
            request.POST,
            event_id=event.id,
            from_player_id=request.user.player.id,
        )

        context["page"] = "#ExtraPoints"

        if form.is_valid():
            extrapoint = ExtraPoint()
            extrapoint.player = form.cleaned_data["player"]
            extrapoint.points = form.cleaned_data["points"]
            extrapoint.description = form.cleaned_data["description"]
            extrapoint.event = event
            extrapoint.from_player = current_player

            extrapoint.save()

            render(request, "gamemanager/event_live.html", context)
        else:
            context["form"] = form
            render(request, "gamemanager/event_live.html", context)

    return render(request, "gamemanager/event_live.html", context)


def extrapoints_delete(request, pk):
    extrapoint = get_object_or_404(ExtraPoint, id=pk)
    event = extrapoint.event

    next_url = request.GET.get("next")

    if next_url:
        redirect_url = redirect("%s" % (next_url))
    else:
        redirect_url = redirect(reverse_lazy("event live", kwargs={"pk": event.id}))

    if extrapoint.from_player != request.user.player or event.status != 2:
        return redirect_url

    extrapoint.delete()
    return redirect_url


def live_extrapoints(request, pk):
    extrapoints = ExtraPoint.objects.filter(event=pk, player=request.user.player)
    context = {}

    context["object_list"] = extrapoints

    return render(request, "gamemanager/event_live_extrapoints.html", context)


# SUPPORT FUNCTIONS
def create_importance_pairs(event):
    games = [x.id for x in event.games.all()]
    asd = []

    for a in list(itertools.permutations(games, 2)):
        b = sorted(list(a))
        b.sort()

        if not b in asd:
            asd.append(b)

    asd.sort(key=itemgetter(0, 1))

    return asd


def calculate_scores(event, score_weights):
    players = event.players.all()
    event_games = event.games.all()
    places = Placement.objects.all().filter(event=event, game__in=event_games)
    extra_points = ExtraPoint.objects.all().filter(event=event)

    points = {
        "firstplace_id": event.firstplace_factor,
        "secondplace_id": event.secondplace_factor,
        "thirdplace_id": event.thirdplace_factor,
    }

    playerscore = {}

    for player in players:
        playerscore[player.id] = 0

    for place in places:
        for field in ["firstplace", "secondplace", "thirdplace"]:
            for player in getattr(place, field).all():
                playerid = player.id
                gameid = place.game.id

                if not playerid in playerscore:
                    continue

                if not playerid:
                    continue

                if not gameid in score_weights:
                    continue

                playerscore[playerid] += (
                    points[field + "_id"] * event.score * score_weights[gameid]
                )

    for extra_point in extra_points:
        playerscore[extra_point.player.id] += extra_point.points

    playerscore = sorted(playerscore.items(), key=lambda x: x[1], reverse=True)

    return playerscore
