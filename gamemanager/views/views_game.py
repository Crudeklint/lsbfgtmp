import os
import requests

from urllib.parse import urlparse, unquote

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core import files
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic

from ..models import Game, Placement, Player
from ..forms import GameCreateUpdateForm, GameImportSteamForm
from .._support_functions import CreateUpdateView, admin_check, admin_or_creator_check


@method_decorator(login_required, name="dispatch")
class GameDetailView(generic.DetailView):
    model = Game

    def get_context_data(self, **kwargs):
        pk = self.kwargs.get("pk")
        game = Game.objects.get(pk=pk)

        placements = []

        for placement in (
            Placement.objects.all().filter(game=game).order_by("event__date")
        ):
            placements.append(
                {
                    "event": placement.event,
                    "fp": placement.firstplace,
                    "sp": placement.secondplace,
                    "tp": placement.thirdplace,
                }
            )

        context = super().get_context_data(**kwargs)
        context["placements"] = placements

        return context


@method_decorator(login_required, name="dispatch")
class GameListView(generic.ListView):
    model = Game

    def get_context_data(self, **kwargs):
        context = {}
        system_groups = {}

        systems = list(
            Game.objects.all()
            .order_by("system")
            .values_list("system", flat=True)
            .distinct()
        )

        for system in systems:
            system_groups[system] = Game.objects.filter(system=system).order_by(
                "system"
            )

        context["object_list"] = system_groups

        return context


class GameCreateUpdateView(CreateUpdateView):
    model = Game
    form_class = GameCreateUpdateForm
    success_url = "/games/"
    create_message = "{} har lagts till!"
    update_message = "{} har uppdaterats"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        player = Player.objects.get(user=self.request.user)

        obj = form.save(commit=False)

        pk = obj.pk

        obj.added_by = player
        obj.save()

        if not pk:
            messages.success(self.request, self.create_message.format(obj))
        else:
            messages.success(self.request, self.update_message.format(obj))

        return super().form_valid(form)

    def get_success_url(self):
        next_url = self.request.GET.get("next")

        if next_url:
            return "%s" % (next_url)
        else:
            return reverse("game list")


class GameDeleteView(generic.DeleteView):
    model = Game
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("game list")
    success_message = "{} har tagits bort."

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()

        messages.success(self.request, self.success_message.format(obj))
        return super().delete(request, *args, **kwargs)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)


class GameImportSteamView(generic.FormView):
    template_name = "gamemanager/game_import_steam_form.html"
    form_class = GameImportSteamForm
    success_url = "/"

    def form_valid(self, form):
        steam_data = form.cleaned_data["steam_href"]
        box_art_href = steam_data["header_image"]
        gameid = steam_data["gameid"]
        file_written = False

        response = requests.get(box_art_href, stream=True)

        if response.status_code == requests.codes.ok:
            parsed_url = urlparse(box_art_href)
            ext = os.path.splitext(os.path.basename(parsed_url.path))[1]
            new_basename = gameid

            dir = settings.MEDIA_ROOT
            newfile_fullpath = os.path.join(dir, new_basename + ext)

            lf = open(newfile_fullpath, "wb+")

            for block in response.iter_content(1024 * 8):
                if not block:
                    break

                lf.write(block)

            file_written = True

        new_game = Game()

        if file_written:
            new_game.boxart.save(new_basename + ext, files.File(lf))
            lf.close()
            os.remove(newfile_fullpath)

        new_game.name = steam_data["name"]
        new_game.system = "PC"
        new_game.youtube = steam_data["movie"]
        new_game.added_by = self.request.user.player
        new_game.save()

        pk = new_game.id

        messages.add_message(
            self.request, messages.SUCCESS, "Data hämtad! Fyll i resten av infon."
        )

        return redirect(reverse("game update", kwargs={"pk": pk}))
