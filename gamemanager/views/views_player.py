import os
import datetime
import random
import string

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import LoginView
from django.db.models import Count
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect, get_object_or_404

from .._support_functions import CreateUpdateView, admin_check, send_token_via_mail
from ..models import Player, PlayerCreateToken, Placement, Event, ExtraPoint, User
from ..forms import PlayerUpdateForm, PlayerUpdatePasswordForm, CustomAuthenticationForm
from ..forms import PlayerCreateWTokenForm, PlayerTokenCreateForm, PlayerCreateForm


@method_decorator(login_required, name="dispatch")
class PlayerDetailView(generic.DetailView):
    model = Player

    def get_context_data(self, **kwargs):
        pk = self.kwargs.get("pk")
        player = Player.objects.get(pk=pk)

        my_extrapoints = (
            ExtraPoint.objects.all().filter(player=player).order_by("from_player")
        )

        my_data = {}

        fps = Placement.objects.all().filter(firstplace=player)
        sps = Placement.objects.all().filter(secondplace=player)
        tps = Placement.objects.all().filter(thirdplace=player)

        placements = [fps, sps, tps]
        classnames = ["firstplace", "secondplace", "thirdplace"]

        for i in range(0, len(placements)):
            for placement in placements[i]:
                event = placement.event

                if not event in my_data:
                    my_data[event] = {}

                game = placement.game
                my_data[event][game] = classnames[i]

        context = super().get_context_data(**kwargs)
        context["placement_info"] = my_data
        context["extrapoints"] = my_extrapoints

        return context


class PlayerUpdateView(generic.FormView):
    form_class = PlayerUpdateForm
    template_name = "gamemanager/player_form.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        pk = self.kwargs.get("pk")

        edit_player = get_object_or_404(Player, id=pk)
        current_user = self.request.user

        if not (edit_player == current_user.player or admin_check(current_user)):
            raise PermissionDenied

        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        pk = self.kwargs.get("pk")

        player = get_object_or_404(Player, id=pk)
        user = player.user

        initial = super().get_initial()
        initial["gamertag"] = player.gamertag
        initial["image"] = player.image
        initial["username"] = user.username
        initial["first_name"] = user.first_name
        initial["last_name"] = user.last_name
        initial["notifications"] = []

        if player.get_event_notifications:
            initial["notifications"].append("event")

        if player.get_news_notifications:
            initial["notifications"].append("news")

        return initial

    def form_valid(self, form):
        pk = self.kwargs.get("pk")
        player = get_object_or_404(Player, id=pk)
        player.gamertag = form.cleaned_data["gamertag"]

        selected_notifications = form.cleaned_data["notifications"]
        image = form.cleaned_data["image"]

        player.get_event_notifications = "event" in selected_notifications
        player.get_news_notifications = "news" in selected_notifications

        if image and player.image != image:
            image_ext = os.path.splitext(image.name)[1]
            filename = os.path.join(
                settings.MEDIA_ROOT,
                "profiles",
                "{}_profile{}".format(player.id, image_ext),
            )

            with open(filename, "wb") as destination:
                for chunk in image.chunks():
                    destination.write(chunk)

            player.image = os.path.join("profiles", os.path.basename(filename))
        elif image == 0:
            player.image.delete(save=True)

        try:
            player.save()
        except IntegrityError:
            messages.error(self.request, "Gamertagen finns redan!")
            return redirect(reverse("player update", kwargs={"pk": pk}))

        user = player.user
        user.first_name = form.cleaned_data["first_name"]
        user.last_name = form.cleaned_data["last_name"]
        user.username = form.cleaned_data["username"]

        try:
            user.save()
        except IntegrityError:
            messages.error(self.request, "Användarnamnet finns redan!")
            return redirect(reverse("player update", kwargs={"pk": pk}))

        messages.success(self.request, "Användaruppgifter ändrade!")

        return redirect(reverse("player detail", kwargs={"pk": pk}))


class PlayerUpdatePasswordView(generic.FormView):
    form_class = PlayerUpdatePasswordForm
    model = Player
    success_url = "/"
    success_message = "Lösenordet uppdaterat!"
    template_name = "gamemanager/player_form.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        pk = self.kwargs.get("pk")

        edit_player = get_object_or_404(Player, id=pk)
        current_user = self.request.user
        current_player = current_user.player

        if not (edit_player == current_player or admin_check(current_user)):
            raise PermissionDenied

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        pk = self.kwargs.get("pk")

        password = form.cleaned_data["password1"]

        user = get_object_or_404(Player, id=pk).user
        user.set_password(password)
        user.save()

        update_session_auth_hash(self.request, user)

        messages.success(self.request, self.success_message)

        return redirect(reverse("player detail", kwargs={"pk": pk}))

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        pk = self.kwargs.get("pk")

        edit_player = get_object_or_404(Player, id=pk)
        current_user = self.request.user

        if not (edit_player == current_user.player or admin_check(current_user)):
            raise PermissionDenied

        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        pk = self.kwargs.get("pk")

        player = get_object_or_404(Player, id=pk)
        user = player.user

        initial = super().get_initial()
        initial["gamertag"] = player.gamertag
        initial["get_notifications"] = player.get_notifications
        initial["username"] = user.username
        initial["first_name"] = user.first_name
        initial["last_name"] = user.last_name
        return initial

    def form_valid(self, form):
        pk = self.kwargs.get("pk")

        player = get_object_or_404(Player, id=pk)
        player.gamertag = form.cleaned_data["gamertag"]
        player.get_notifications = form.cleaned_data["get_notifications"]

        try:
            player.save()
        except IntegrityError:
            messages.error(self.request, "Gamertagen finns redan!")
            return redirect(reverse("player update", kwargs={"pk": pk}))

        user = player.user
        user.first_name = form.cleaned_data["first_name"]
        user.last_name = form.cleaned_data["last_name"]
        user.username = form.cleaned_data["username"]

        try:
            user.save()
        except IntegrityError:
            messages.error(self.request, "Användarnamnet finns redan!")
            return redirect(reverse("player update", kwargs={"pk": pk}))

        messages.success(self.request, "Användaruppgifter ändrade!")

        return redirect(reverse("player detail", kwargs={"pk": pk}))


class PlayerUpdatePasswordView(generic.FormView):
    form_class = PlayerUpdatePasswordForm
    model = Player
    success_url = "/"
    success_message = "Lösenordet uppdaterat!"
    template_name = "gamemanager/player_form.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        pk = self.kwargs.get("pk")

        edit_player = get_object_or_404(Player, id=pk)
        current_user = self.request.user
        current_player = current_user.player

        if not (edit_player == current_player or admin_check(current_user)):
            raise PermissionDenied

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        pk = self.kwargs.get("pk")

        password = form.cleaned_data["password1"]

        user = get_object_or_404(Player, id=pk).user
        user.set_password(password)
        user.save()

        update_session_auth_hash(self.request, user)

        messages.success(self.request, self.success_message)

        return redirect(reverse("player detail", kwargs={"pk": pk}))


def player_create(request, token=None):
    if not token and not admin_check(request.user):
        raise PermissionDenied

    pre_mail = ""

    if request.method == "POST":
        if token:
            form = PlayerCreateWTokenForm(request.POST)
        else:
            form = PlayerCreateForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]

            gamertag = form.cleaned_data["gamertag"]
            last_name = form.cleaned_data["last_name"]
            first_name = form.cleaned_data["first_name"]
            password = form.cleaned_data["password1"]
            verified_mail = form.cleaned_data["verified_mail"]

            token = form.cleaned_data["token"]

            user = User()
            user.username = username
            user.first_name = first_name
            user.last_name = last_name
            user.set_password(password)
            user.save()

            player = Player()
            player.gamertag = gamertag
            player.user = user
            player.verified_mail = verified_mail
            player.save()

            if token:
                try:
                    token = PlayerCreateToken.objects.get(token=token)
                    token.delete()
                except PlayerCreateToken.DoesNotExist:
                    a = 0

                return redirect(reverse_lazy("login"))

            return redirect(reverse_lazy("player list"))

    else:
        if token:
            found_token = PlayerCreateToken.objects.filter(
                token=token, expire_date__gt=datetime.datetime.now()
            ).first()

            if found_token == None:
                raise (PermissionDenied)

            form = PlayerCreateWTokenForm(
                initial={"username": found_token.email, "token": token}
            )
        else:
            form = PlayerCreateForm()

    context = {"form": form}

    return render(request, "gamemanager/player_form.html", context)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class PlayerDeleteView(generic.DeleteView):
    model = Player
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("player list admin")


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class PlayerAdminListView(generic.ListView):
    model = Player
    template_name = "gamemanager/player_list_admin.html"


@method_decorator(login_required, name="dispatch")
class PlayerListView(generic.ListView):
    model = Player

    def get_queryset(self):
        # queryset = Player.objects.filter(event__isnull=False).distinct()
        queryset = (
            Player.objects.filter(event__isnull=False)
            .annotate(latest_event_date=Count("event__date"))
            .order_by("-latest_event_date")
        )
        return queryset


class CustomLoginView(LoginView):
    authentication_form = CustomAuthenticationForm


def custom_logout(request):
    logout(request)
    return render(request, "registration/logged_out.html")


# +----------------------+
# | PLAYER-CREATE TOKENS |
# +----------------------+


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class PlayerTokenCreateView(generic.CreateView):
    model = PlayerCreateToken
    success_url = reverse_lazy("usertoken list")
    form_class = PlayerTokenCreateForm
    template_name = "gamemanager/usercreatetoken_form.html"

    def form_valid(self, form):
        tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
        token = "".join(random.choices(string.ascii_uppercase + string.digits, k=30))

        obj = form.save(commit=False)
        obj.expire_date = tomorrow
        obj.token = token

        email = obj.email

        obj.save()

        send_token_via_mail(token, email)

        return super().form_valid(form)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class PlayerTokenListView(generic.ListView):
    model = PlayerCreateToken
    template_name = "gamemanager/usercreatetoken_list.html"


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class PlayerTokenDeleteView(generic.DeleteView):
    model = PlayerCreateToken
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("usertoken list")
