from django.views import generic
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test

from ..models import BugReport
from .._support_functions import CreateUpdateView, admin_or_creator_check
from ..forms import FoodCreateUpdateForm


class BugCreateUpdateView(CreateUpdateView):
    model = BugReport
    fields = ["report"]
    success_url = reverse_lazy("bugreport list")

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        player = self.request.user.player

        obj = form.save(commit=False)
        obj.added_by = player
        obj.save()

        return super().form_valid(form)


@method_decorator(login_required, name="dispatch")
class BugListView(generic.ListView):
    model = BugReport


class BugDeleteView(generic.DeleteView):
    model = BugReport
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("bugreport list")

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        user = self.request.user
        obj = self.get_object()

        admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)
