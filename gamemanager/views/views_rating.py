from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect

from ..models import Rating


@login_required
def rating_view(request, model_str, pk, rating):
    create_message = "Ditt betyg för {} har lagts till!"
    update_message = "Ditt betyg för {} har uppdaterats!"
    delete_message = "Ditt betyg för {} har tagits bort!"

    return_redirect = HttpResponseRedirect(request.META.get("HTTP_REFERER"))

    delete = False
    rating = int(rating)

    try:
        model_obj = apps.get_model(app_label="gamemanager", model_name=model_str)
    except LookupError:
        messages.error(
            request, "Det gick inte att hitta objektet du försöker betygssätta!"
        )
        return return_redirect

    player = request.user.player
    object = model_obj.objects.get(pk=pk)
    content_type = ContentType.objects.get_for_model(object)

    try:
        rating_req = Rating.objects.get(
            content_type=content_type, object_id=pk, player=player
        )

        if rating == 0:
            messages.success(request, delete_message.format(str(object)))
            delete = True
        else:
            messages.success(request, update_message.format(str(object)))
            rating_req.rating = rating

    except Rating.DoesNotExist:
        messages.success(request, create_message.format(str(object)))
        rating_req = Rating(
            content_type=content_type, object_id=pk, rating=rating, player=player
        )

    if delete:
        rating_req.delete()
    else:
        rating_req.save()

    return return_redirect
