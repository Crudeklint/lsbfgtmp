from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic

from ..models import News
from .._support_functions import (
    CreateUpdateView,
    admin_check,
    admin_or_creator_check,
    send_news_via_mail,
)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class NewsUpdateCreateView(CreateUpdateView):
    model = News
    success_url = reverse_lazy("news list")
    fields = ("heading", "article_text")

    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form, **kwargs):
        player = self.request.user.player

        obj = form.save(commit=False)

        pk = obj.pk

        obj.added_by = player
        obj.save()

        if not pk:
            send_news_via_mail(obj)

        return super().form_valid(form, **kwargs)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class NewsListView(generic.ListView):
    model = News
    exclude = ("date", "added_by")


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class NewsDeleteView(generic.DeleteView):
    model = News
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("news list")


@method_decorator(login_required, name="dispatch")
class NewsDetailView(generic.DetailView):
    model = News
