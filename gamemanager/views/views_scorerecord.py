import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Sum, Avg
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic


from ..models import ScoreRecord, Event, Player
from .views_event import calculate_importances, calculate_scores


@method_decorator(login_required, name="dispatch")
class ScoreRecordListView(generic.ListView):
    model = ScoreRecord
    template_name = "gamemanager/score_list.html"

    def get_context_data(self, **kwargs):
        if not "year" in self.kwargs:
            year = datetime.datetime.now().year
        else:
            year = self.kwargs["year"]

        context = super().get_context_data(**kwargs)

        event_creators = []
        attends = []
        summed_scores = []

        events = Event.objects.filter(date__year=year)
        players = Player.objects.all().filter(event__in=events).distinct()
        scores = ScoreRecord.objects.filter(event__date__year=year)

        for player in players:
            no_of_events = events.filter(added_by=player).count()
            no_of_attends = events.filter(players=player).count()
            player_scores = scores.filter(player=player).aggregate(
                score_sum=Sum("score")
            )

            if player_scores["score_sum"] == None:
                player_scores["score_sum"] = 0

            event_creators.append({"player": player, "total": no_of_events})
            attends.append({"player": player, "total": no_of_attends})
            summed_scores.append(
                {"player": player, "total": player_scores["score_sum"]}
            )

        event_creators.sort(key=lambda i: i["total"], reverse=True)
        attends.sort(key=lambda i: i["total"], reverse=True)

        context["attends"] = attends
        context["event_creators"] = event_creators
        context["scores"] = sorted(
            summed_scores, key=lambda d: d["total"], reverse=True
        )
        context["year"] = year

        return context


@login_required
def score_record_insert(request, pk):
    event = get_object_or_404(Event, pk=pk)
    player = request.user.player

    if not (event.added_by == player or admin_check(request.user)):
        raise (PermissionDenied)

    if not event.status in [3, 4]:
        messages.add_message(
            request,
            messages.ERROR,
            "Eventet måste vara färdigt för att kunna föra in poäng.",
        )

        return redirect(reverse_lazy("event live admin", kwargs={"pk": event.id}))

    weights = calculate_importances(event)
    scores = calculate_scores(event, weights)

    errors = []

    for score_record in scores:
        score = score_record[1]
        player_id = score_record[0]

        try:
            player = Player.objects.get(pk=player_id)
        except Player.DoesNotExist:
            errors.append(
                'Spelare med id "{}" finns inte i databasen. Kan inte föra in poäng!'.format(
                    score[0]
                )
            )
            continue

        new_score, created = ScoreRecord.objects.get_or_create(
            player=player, event=event, defaults={"score": 0}
        )

        new_score.score = int(score)
        new_score.save()

    next_url = request.GET.get("next")

    messages.add_message(request, messages.SUCCESS, "Poäng sparad!")

    if next_url:
        return redirect("%s" % (next_url))
    else:
        return redirect(reverse_lazy("event live admin", kwargs={"pk": event.id}))
