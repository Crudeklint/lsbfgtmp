from django.views import generic
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test

from .._support_functions import CreateUpdateView, admin_check, admin_or_creator_check
from ..models import MoneyTransaction, Player
from ..forms import MoneyTransactionCreateUpdateForm


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(admin_check), name="dispatch")
class MoneyTransactionUpdateCreateView(CreateUpdateView):
    model = MoneyTransaction
    form_class = MoneyTransactionCreateUpdateForm  # GameCreateUpdateForm
    success_url = reverse_lazy("moneytransactions list")
    create_message = "Betalningen {} har lagts till!"
    update_message = "Betalningen {} har uppdaterats!"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        player = Player.objects.get(user=self.request.user)

        obj = form.save(commit=False)

        pk = obj.pk

        obj.save()

        if not pk:
            messages.success(self.request, self.create_message.format(obj))
        else:
            messages.success(self.request, self.update_message.format(obj))

        return super().form_valid(form)


@method_decorator(login_required, name="dispatch")
class MoneyTransactionListView(generic.ListView):
    model = MoneyTransaction

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        transactions = MoneyTransaction.objects.all().order_by("-date")

        total = sum([x.amount * x.direction for x in transactions])

        context["object_list"] = transactions
        context["sum"] = total

        return context
