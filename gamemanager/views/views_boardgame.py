import requests
import os

from urllib.parse import urlparse, unquote

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core import files
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic

from ..models import BoardGame, Player
from .._support_functions import CreateUpdateView, admin_check, admin_or_creator_check
from ..forms import BoardGameCreateUpdateForm, BoardGameImportBGGForm


@method_decorator(login_required, name="dispatch")
class BoardGameDetailView(generic.DetailView):
    model = BoardGame


@method_decorator(login_required, name="dispatch")
class BoardGameListView(generic.ListView):
    model = BoardGame
    template_name = "gamemanager/boardgame_list.html"


class BoardGameCreateUpdateView(CreateUpdateView):
    model = BoardGame
    form_class = BoardGameCreateUpdateForm
    success_url = "/boardgames/"
    create_message = "{} har lagts till!"
    update_message = "{} har uppdaterats"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        if obj != None:
            admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        player = Player.objects.get(user=self.request.user)

        obj = form.save(commit=False)

        pk = obj.pk

        obj.added_by = player
        obj.save()

        if not pk:
            messages.success(self.request, self.create_message.format(obj))
        else:
            messages.success(self.request, self.update_message.format(obj))

        return super().form_valid(form)

    def get_success_url(self):
        next_url = self.request.GET.get("next")

        if next_url:
            return "%s" % (next_url)
        else:
            return reverse("boardgame list")


class BoardGameDeleteView(generic.DeleteView):
    model = BoardGame
    template_name = "gamemanager/delete_confim.html"
    success_url = reverse_lazy("boardgame list")
    success_message = "{} har tagits bort."

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()

        messages.success(self.request, self.success_message.format(obj))
        return super().delete(request, *args, **kwargs)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user

        admin_or_creator_check(user, obj)

        return super().dispatch(*args, **kwargs)


class BoardGameImportBGGView(generic.FormView):
    template_name = "gamemanager/boardgame_import_bgg_form.html"
    form_class = BoardGameImportBGGForm
    success_url = "/"

    def form_valid(self, form):
        bgg_data = form.cleaned_data["bgg_href"]
        box_art_href = bgg_data["header_image"]
        gameid = bgg_data["gameid"]
        file_written = False

        response = requests.get(box_art_href, stream=True)

        if response.status_code == requests.codes.ok:
            parsed_url = urlparse(box_art_href)
            ext = os.path.splitext(os.path.basename(parsed_url.path))[1]
            new_basename = gameid

            dir = settings.MEDIA_ROOT
            newfile_fullpath = os.path.join(dir, new_basename + ext)

            lf = open(newfile_fullpath, "wb+")

            for block in response.iter_content(1024 * 8):
                if not block:
                    break

                lf.write(block)

            file_written = True

        new_boardgame = BoardGame()

        if file_written:
            new_boardgame.boxart.save(new_basename + ext, files.File(lf))
            lf.close()
            os.remove(newfile_fullpath)

        new_boardgame.name = bgg_data["name"]
        new_boardgame.description = bgg_data["description"].replace("&#10;", "\n")
        new_boardgame.no_of_players = bgg_data["max_players"]
        new_boardgame.added_by = self.request.user.player
        new_boardgame.owner = self.request.user.player
        new_boardgame.save()

        pk = new_boardgame.id

        messages.add_message(
            self.request, messages.SUCCESS, "Data hämtad! Fyll i resten av infon."
        )

        return redirect(reverse("boardgame update", kwargs={"pk": pk}))


# +------------------+
# | BUGS/SUGGESTIONS |
# +------------------+
