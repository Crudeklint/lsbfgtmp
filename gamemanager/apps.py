from django.apps import AppConfig


class GamemanagerConfig(AppConfig):
    name = "gamemanager"
    default_auto_field = "django.db.models.AutoField"
