import re

from django import template
from django.contrib.contenttypes.models import ContentType
from django.db.models import Avg
from django.template import RequestContext
from gamemanager.models import Rating

register = template.Library()


@register.inclusion_tag("gamemanager/_event_block.html", takes_context=True)
def render_event_block(context, event):
    return {"event": event, "user": context["request"].user}


@register.inclusion_tag("gamemanager/_rating_block.html", takes_context=True)
def show_ratings2(context, object, player, simple=False):
    content_type = ContentType.objects.get_for_model(object)
    model_str = object._meta.model.__name__

    object_ratings = Rating.objects.filter(
        content_type=content_type, object_id=object.id
    )

    try:
        my_rating = object_ratings.filter(player=player).values()[0]["rating"]
    except (IndexError, ValueError):
        my_rating = 0

    try:
        avg_rating = int(
            round(object_ratings.aggregate(Avg("rating"))["rating__avg"], 0)
        )
    except TypeError:
        avg_rating = 0

    return {
        "model_str": model_str,
        "my_rating": my_rating,
        "avg_rating": avg_rating,
        "object_id": object.id,
        "simple": simple,
    }


@register.inclusion_tag("gamemanager/_rating_block.html", takes_context=True)
def show_ratings1(context, points, simple=False):
    return {
        "my_rating": -2,
        "avg_rating": points,
        "simple": simple,
        "points_only": True,
    }
