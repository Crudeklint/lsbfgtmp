from django import template
from django.template.loader import render_to_string

register = template.Library()

import calendar
import datetime

import gamemanager
from gamemanager.models import Event, MoneyTransaction


class EventCalendar(calendar.HTMLCalendar):
    translations = ["M", "T", "O", "T", "F", "L", "S"]

    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super(EventCalendar, self).__init__()

    def formatmonthname(self, theyear, themonth, withyear):
        months = [
            "invalid",
            "Januari",
            "Februari",
            "Mars",
            "April",
            "Maj",
            "Juni",
            "Juli",
            "Augusti",
            "September",
            "Oktober",
            "November",
            "December",
        ]

        return "<tr><th class='month' colspan='7'>%s</th></tr>" % months[themonth]

    def formatweekheader(self):
        v = []
        a = v.append
        a("<tr>")

        for i in range(self.firstweekday, self.firstweekday + 7):
            a("<th>%s</th>" % (self.translations[i % 7]))
        # print( self.firstweekday )

        return "".join(v)

    def formatday(self, date_row):
        """
        Return a day as a table cell.
        """
        id = "calendar_%d-%02d-%02d" % (date_row.year, date_row.month, date_row.day)

        if date_row.month != self.month:
            return '<td class="noday">&nbsp;</td>'  # day outside month
        else:
            return '<td id="%s" class="%s">%d</td>' % (
                id,
                self.cssclasses[date_row.weekday()],
                date_row.day,
            )

    def formatweek(self, theweek):
        """
        Return a complete week as a table row.
        """
        s = "".join(self.formatday(date_row) for date_row in theweek)
        return "<tr>%s</tr>" % s

    def formatmonth(self, theyear, themonth, withyear=True):
        """
        Return a formatted month as a table.
        """
        v = []
        a = v.append
        a('<table class="calendar">')
        a("\n")
        a(self.formatmonthname(theyear, themonth, withyear=withyear))
        a("\n")
        a(self.formatweekheader())
        a("\n")
        dates = list(self.itermonthdates(theyear, themonth))
        self.month = themonth
        records = [dates[i : i + 7] for i in range(0, len(dates), 7)]
        for week in records:
            a(self.formatweek(week))
            a("\n")
        a("</table>")
        a("\n")
        return "".join(v)

    def formatyear(self, theyear, width=3):
        """
        Return a formatted year as a table of tables.
        """
        start_month = int(datetime.datetime.today().month) - 1

        v = []
        a = v.append
        width = max(width, 1)
        # a('<table border="0" cellpadding="0" cellspacing="0" class="year">')
        # a('\n')
        for i in range(start_month, start_month + 3, width):
            if i < 1:
                continue
            # months in this row
            months = range(i, min(i + width, 13))
            # a('<tr>')
            for m in months:
                # a('<td>')
                a(self.formatmonth(theyear, m, withyear=False))
                # a('</td>')
            # a('</tr>')
        # a('</table>')
        return "".join(v)


@register.simple_tag
def side_bar_content():
    myCal = EventCalendar(calendar.MONDAY)
    cal_html = myCal.formatyear(datetime.date.today().year, 1)

    events = Event.objects.all()

    transactions = MoneyTransaction.objects.all().order_by("-date")
    total = sum([x.amount * x.direction for x in transactions])

    return render_to_string(
        "gamemanager/_sidebar.html",
        {"cal": cal_html, "events": events, "account_sum": total},
    )
