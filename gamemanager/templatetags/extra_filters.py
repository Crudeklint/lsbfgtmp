import markdown as md
import re

from django import template
from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import stringfilter
from django.urls import reverse, reverse_lazy
from django.urls.exceptions import NoReverseMatch
from gamemanager.models import Group

register = template.Library()


@register.simple_tag
def field_name(instance, field_name):
    """
    Returns verbose_name for a field.
    """
    return instance._meta.get_field(field_name).verbose_name.title()


@register.filter
def lookup(d, key):
    if key not in d:
        return None
    return d[key]


@register.filter
def prettynone(value):
    if value == None:
        return " - "
    return value


@register.filter
def is_admin(user):
    group = Group.objects.filter(name="admins").first()
    return group in user.groups.all()


@register.filter()
@stringfilter
def markdown(value):
    return md.markdown(value, extensions=["markdown.extensions.fenced_code"])


@register.filter(name="times")
def times(number):
    return range(1, number + 1)


@register.simple_tag
def get_object_name(modelstr, pk):
    modelstr = str(modelstr)
    model = None
    pk = int(pk)

    try:
        model = apps.get_model(app_label="gamemanager", model_name=modelstr)

        if model == None:
            return None

        object = model.objects.get(id=pk)
        return str(object)
    except (ObjectDoesNotExist, LookupError):
        return None


@register.simple_tag
def get_object(modelstr, pk):
    modelstr = str(modelstr)
    model = None
    pk = int(pk)

    try:
        model = apps.get_model(app_label="gamemanager", model_name=modelstr)

        if model == None:
            return None

        object = model.objects.get(id=pk)
        return object
    except (ObjectDoesNotExist, LookupError):
        return None


@register.simple_tag
def get_object_link(modelstr, pk):
    modelstr = str(modelstr)
    urlname = modelstr + " detail"

    try:
        url = reverse(urlname, kwargs={"pk": pk})
        return url

    except NoReverseMatch:
        return "#"


@register.filter
def break_dash(value):
    return value.replace(" - ", "\n")


@register.filter
def remove_cob(value):
    match = re.search("chili och brädspel", value, re.IGNORECASE)

    if match:
        return value[match.end() + 1 :]
    else:
        return value
