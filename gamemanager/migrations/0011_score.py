# Generated by Django 2.2 on 2021-08-15 14:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0010_auto_20210808_1145'),
    ]

    operations = [
        migrations.CreateModel(
            name='Score',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.IntegerField(verbose_name='Poäng')),
                ('event', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='scorerecord_event', to='gamemanager.Event')),
                ('player', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gamemanager.Player', verbose_name='Spelare')),
            ],
        ),
    ]
