# Generated by Django 2.2 on 2023-01-31 19:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0043_auto_20230131_1819'),
    ]

    operations = [
        migrations.DeleteModel(
            name='GameRating',
        ),
    ]
