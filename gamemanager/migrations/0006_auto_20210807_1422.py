# Generated by Django 2.2 on 2021-08-07 12:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0005_auto_20210807_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bugreport',
            name='added_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Datum'),
        ),
        migrations.AlterField(
            model_name='playercreatetoken',
            name='expire_date',
            field=models.DateTimeField(null=True),
        ),
    ]
