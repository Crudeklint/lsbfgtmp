# Generated by Django 2.2 on 2022-03-01 22:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0037_auto_20220301_2131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extrapoint',
            name='from_player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scorerecord_event', to='gamemanager.Player', verbose_name='Från spelare'),
        ),
    ]
