# Generated by Django 2.2 on 2023-09-24 01:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0047_remove_event_qr'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='get_notifications',
        ),
        migrations.AddField(
            model_name='player',
            name='get_event_notifications',
            field=models.BooleanField(default=False, verbose_name='Vill ha event-notifikationer'),
        ),
        migrations.AddField(
            model_name='player',
            name='get_news_notifications',
            field=models.BooleanField(default=False, verbose_name='Vill ha nyhetsnotifikationer'),
        ),
        migrations.AddField(
            model_name='player',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='player_images'),
        ),
        migrations.AlterField(
            model_name='event',
            name='bracket_data',
            field=models.TextField(blank=True, null=True, verbose_name='Bracket data'),
        ),
    ]
