# Generated by Django 2.2 on 2021-08-07 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0006_auto_20210807_1422'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='playercreatetoken',
            name='id',
        ),
        migrations.AlterField(
            model_name='playercreatetoken',
            name='token',
            field=models.CharField(default='000000000000000000000000000000', max_length=30, primary_key=True, serialize=False),
        ),
    ]
