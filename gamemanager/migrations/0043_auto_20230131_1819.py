# Generated by Django 2.2 on 2023-01-31 18:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0042_auto_20230131_1759'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='moneytransaction',
            name='from_player',
        ),
        migrations.AddField(
            model_name='moneytransaction',
            name='player',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='player', to='gamemanager.Player', verbose_name='spelare'),
        ),
        migrations.AlterField(
            model_name='moneytransaction',
            name='direction',
            field=models.IntegerField(choices=[(-1, 'Från LSBFGT'), (1, 'Till LSBFGT')], verbose_name='Riktning'),
        ),
    ]
