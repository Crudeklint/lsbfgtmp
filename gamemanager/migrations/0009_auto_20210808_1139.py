# Generated by Django 2.2 on 2021-08-08 09:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamemanager', '0008_auto_20210808_1138'),
    ]

    operations = [
        migrations.AddField(
            model_name='playercreatetoken',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='playercreatetoken',
            name='token',
            field=models.CharField(default='000000000000000000000000000000', max_length=30),
        ),
    ]
